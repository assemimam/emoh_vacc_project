//
//  AppDelegate.h
//  emoh_vacc
//
//  Created by Assem Imam on 12/24/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MONActivityIndicatorView.h"
#define SHOW_LOADING [(AppDelegate*)[[UIApplication sharedApplication]delegate] showLoadingView:YES ];
#define HIDE_LOADING [ (AppDelegate*)[[UIApplication sharedApplication]delegate] showLoadingView:NO];

@interface AppDelegate : UIResponder <UIApplicationDelegate,MONActivityIndicatorViewDelegate>
{
    __weak IBOutlet UIView *LoadingView;
    __weak IBOutlet MONActivityIndicatorView *LoadingActivityIndicator;
    UIAlertView *reachiblityAlertView;
    NSString *reachabilityObserver;
}
@property (strong, nonatomic) IBOutlet UIWindow *window;
@property (weak, nonatomic) IBOutlet UINavigationController *AppNavigationController;
-(void)showLoadingView:(BOOL)show;
@end
