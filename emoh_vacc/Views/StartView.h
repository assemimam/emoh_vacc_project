//
//  StartView.h
//  emoh_vacc
//
//  Created by iOS Developer on 1/4/15.
//  Copyright (c) 2015 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StartView : UIView


@property (nonatomic ,weak) IBOutlet UIImageView *img;
@property (nonatomic ,weak) IBOutlet UILabel *lbl;

+(instancetype)setupStartView;
@end
