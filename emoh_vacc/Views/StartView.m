//
//  StartView.m
//  emoh_vacc
//
//  Created by iOS Developer on 1/4/15.
//  Copyright (c) 2015 Nuitex. All rights reserved.
//

#import "StartView.h"

@implementation StartView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+(instancetype)setupStartView
{
    return [[NSBundle mainBundle] loadNibNamed:@"StartView"
                                         owner:self
                                       options:nil][0];
}
@end
