//
//  Child.h
//  emoh_vacc
//
//  Created by iOS Developer on 12/28/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Child : NSObject
@property (nonatomic) int childId;
@property (nonatomic ,strong) NSString *childName;
@property (nonatomic) int childGender;
@property (nonatomic ,strong) NSString *childBirthdDate;
@property (nonatomic ,strong) NSString *childPhoto;

@end
