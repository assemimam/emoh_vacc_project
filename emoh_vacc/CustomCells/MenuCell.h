//
//  MenuCell.h
//  emoh_vacc
//
//  Created by Assem Imam on 12/30/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *MenuItemLabel;
@property (weak, nonatomic) IBOutlet UIImageView *MenuItemImageView;

@end
