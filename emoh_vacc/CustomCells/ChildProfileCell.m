//
//  ChildProfileCell.m
//  emoh_vacc
//
//  Created by iOS Developer on 1/5/15.
//  Copyright (c) 2015 Nuitex. All rights reserved.
//

#import "ChildProfileCell.h"

@implementation ChildProfileCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+(instancetype)setUpChildProfileCell
{
    return [[NSBundle mainBundle]loadNibNamed:@"ChildProfileCell"
                                        owner:self
                                      options:nil][0];
}
@end
