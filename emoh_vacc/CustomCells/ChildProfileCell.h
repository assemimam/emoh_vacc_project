//
//  ChildProfileCell.h
//  emoh_vacc
//
//  Created by iOS Developer on 1/5/15.
//  Copyright (c) 2015 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChildProfileCell : UITableViewCell

@property (nonatomic ,weak) IBOutlet UILabel     *VaccTypeLabel;
@property (nonatomic ,weak) IBOutlet UILabel     *VaccDateLabel;
@property (nonatomic ,weak) IBOutlet UIImageView *VaccImageView;

+(instancetype)setUpChildProfileCell;
@end
