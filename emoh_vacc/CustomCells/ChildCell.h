//
//  ChildCell.h
//  emoh_vacc
//
//  Created by Assem Imam on 12/30/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NPRImageView.h"
#import "SWTableViewCell.h"
@interface ChildCell : SWTableViewCell
{
    
 
}
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *LoadingActivityIndicator;
@property (weak, nonatomic) IBOutlet NPRImageView *ChildGenderImageView;
@property (weak, nonatomic)  IBOutlet UIView *VaccinationsContainerView;
@property (weak, nonatomic) IBOutlet NPRImageView *ChildImageView;
@property (weak, nonatomic) IBOutlet UILabel *ChildNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *BirthDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *NoOfVaccinationsLabel;

@end
