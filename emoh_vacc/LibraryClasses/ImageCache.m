//
//  ImageCache.m
//  InstrumentsTutorial
//
//  Created by Matt Galloway on 06/09/2012.
//  Copyright (c) 2012 Swipe Stack Ltd. All rights reserved.
//

#import "ImageCache.h"

@interface ImageCache () {
    NSMutableDictionary *_cache;
}
@end

@implementation ImageCache

+ (id)sharedInstance {
    static ImageCache *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[self class] alloc] init];
    });
    return sharedInstance;
}
- (id)init {
    if ((self = [super init])) {
        _cache = [NSMutableDictionary new];
    }
    
    return self;
}

- (UIImage*)imageForKey:(NSString*)key {
    if (key!=nil) {
        return [_cache objectForKey:key];
    }
    else
    {
        return  nil;
    }
    
    
}
- (void)setImage:(UIImage*)image forKey:(NSString*)key {
    @try {
        if (key!=nil & image!=nil) {
            [_cache setObject:image forKey:key];
        }
    }
    @catch (NSException *exception) {
        
    }
   
    
  
}
- (void)downloadImageAtURL:(NSURL*)url completionHandler:(ImageCacheDownloadCompletionHandler)completion {
    @try {
        if (url==nil) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil);
            });
            //NSLog(@"nil URl%@",url);
            return;
        }
        if ([url absoluteString]==nil ) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil);
            });
            //NSLog(@"nil URl%@",url);
            return;
        }
        UIImage *cachedImage = [self imageForKey:[url absoluteString]];
        if (cachedImage) {
            completion(cachedImage);
        } else {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSData *data = [NSData dataWithContentsOfURL:url];
                if (data) {
                    if ([data length]>0 && [data length]/1024<500) {
                        UIImage *image = [UIImage imageWithData:data];
                        [self setImage:image forKey:[url absoluteString]];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(image);
                        });
                        
                    }
                    else{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(nil);
                        });
                        
                    }
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(nil);
                    });
                    
                }
            });
        }

    }
    @catch (NSException *exception) {
        
    }
 
   }

@end
