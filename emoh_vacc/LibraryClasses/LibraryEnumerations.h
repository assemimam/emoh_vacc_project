//
//  LibraryEnumerations.h
//  More
//
//  Created by Hani on 9/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#ifndef _H_LibraryEnumerations
#define _H_LibraryEnumerations

// Language
typedef enum {
	languageArabic = 1,
	languageEnglish,
} availableLanguages;

typedef enum {
	directionRTL = 1,
	directionLTR = 2
} languageDirection;

typedef enum {
    faceBookMediaType = 0,
    twitterMediaType = 1,
    googlePlusMediaType =2,
    microsoftLiveMediaType = 3
} socialMediaType;

#endif