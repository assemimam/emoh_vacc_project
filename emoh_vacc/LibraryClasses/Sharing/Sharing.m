//
//  Sharing.m
//  emoh_vacc
//
//  Created by Assem Imam on 12/25/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "Sharing.h"


@interface Sharing ()<LiveAuthDelegate, LiveOperationDelegate,GPPSignInDelegate>
{
    LiveConnectClient *liveClient;
    id liveUserData;
}
@end
Sharing *sharingObject = nil;
@implementation Sharing
@synthesize delegate,currentSharingViewController;
+ (Sharing *)getObject{
    @try {
        if (sharingObject == nil) {
            sharingObject = [[Sharing alloc] init];
        }
    }
    @catch (NSException *exception) {
        sharingObject=nil;
    }
    
    return sharingObject;
}
#pragma -mark Live Methods
-(void)liveLoginWithApplicationClientID:(NSString *)app_client_id
{
    @try {
        liveUserData = [[NSMutableDictionary alloc]init];
        liveClient = [[LiveConnectClient alloc] initWithClientId:app_client_id  delegate:self userState:@"initialize"];
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark live authorization delegate
- (void)authCompleted:(LiveConnectSessionStatus) status
              session:(LiveConnectSession *) session
            userState:(id) userState
{
    @try {
        if ([userState isEqual:@"initialize"])
        {
            [liveClient login:self.currentSharingViewController
                       scopes:[NSArray arrayWithObjects:@"wl.signin", nil]  delegate:self userState:@"signin"];
        }
        if ([userState isEqual:@"signin"])
        {
            if (session != nil)
            {
                [liveClient getWithPath:@"me"  delegate:self  userState:@"getMe"];
                
                
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (void)authFailed:(NSError *) error userState:(id)userState
{
    @try {
        if (self.delegate) {
            [self.delegate liveAuthenticationFailedWithError: error];
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark live get user info delegate
- (void) liveOperationSucceeded:(LiveOperation *)operation
{
    @try {
        if ([operation.userState isEqual:@"getMe"]) {
            [liveUserData setObject:[[operation  result] objectForKey:@"name"] forKey:@"name"];
            [liveUserData setObject:[[operation  result] objectForKey:@"id"] forKey:@"id"];
            [liveClient getWithPath:@"me/picture"  delegate:self  userState:@"me-picture"];
        }
        else if ([operation.userState isEqual:@"me-picture"])
        {
            
            [liveUserData setObject:[operation.result objectForKey:@"location"] forKey:@"image"];
            
            if (self.delegate) {
                [self.delegate liveGetUserInformationSucceeded:liveUserData];
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

- (void) liveOperationFailed:(NSError *)error operation:(LiveOperation *)operation
{
    @try {
        if ([operation.userState isEqual:@"getMe"]) {
            if (self.delegate) {
                [self.delegate liveGetUserInformationFailedWithError:error];
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark twitter methods
-(void)twitterLoginWithCustomerKey:(NSString *)customer_key CustomerSecret:(NSString *)customer_secret
{
    @try {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            [[Twitter sharedInstance] startWithConsumerKey:customer_key
                                            consumerSecret:customer_secret];
            [Fabric with:@[[Twitter sharedInstance]]];
            [[Twitter sharedInstance] logInWithCompletion:^
             (TWTRSession *session, NSError *error) {
                 if (session) {
                     NSLog(@"signed in as %@", [session userName]);
                     
                     
                     [[[Twitter sharedInstance] APIClient] loadUserWithID:session.userID
                                                               completion:^(TWTRUser *user,
                                                                            NSError *error)
                      {
                          NSMutableDictionary *user_info = [NSMutableDictionary new];
                          [user_info setObject:[session userName] forKey:@"userName"];
                          [user_info setObject:[session userID] forKey:@"userID"];
                          [user_info setObject:[session authToken] forKey:@"authToken"];
                          [user_info setObject:[session authTokenSecret] forKey:@"authTokenSecret"];
                          [user_info setObject:user.profileImageURL forKey:@"image"];
                          if (self.delegate) {
                              [self.delegate twitterGetUserInformationSucceeded:user_info ];
                          }
                          // handle the response or error
                      }];
                     
                     
                     
                 } else {
                     if (self.delegate) {
                         [self.delegate twitterGetUserInformationFailedWithError:error];
                     }
                 }
             }];
        }
        else{
            if (self.delegate) {
                [self.delegate twitterAuthenticationFailedWithError:nil];
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark google plus methods
-(void)googleLoginWithClientID:(NSString *)client_id
{
    @try {
        GPPSignIn *signIn = [GPPSignIn sharedInstance];
        signIn.clientID = client_id;
        NSArray *scope = [NSArray arrayWithObjects:kGTLAuthScopePlusMe, nil];
        signIn.shouldFetchGooglePlusUser = YES;
        signIn.scopes =scope; // defined in GTLPlusConstants.h
        signIn.delegate = self;
        
        [signIn authenticate];    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark google plus delegate
- (void)finishedWithAuth:(GTMOAuth2Authentication *)auth
                   error:(NSError *)error
{
    @try {
        if (error) {
            if (self.delegate) {
                [self.delegate googleAuthenticationFailedWithError:error];
            }
        }
        else{
            if ([GPPSignIn sharedInstance].authentication != nil) {
                NSMutableDictionary *user_info = [NSMutableDictionary new];
                GTLPlusPerson *person = [GPPSignIn sharedInstance].googlePlusUser;
                if (person != nil) {
                    [user_info setObject:person.displayName forKey:@"name"];
                    [user_info setObject: person.gender forKey:@"gender"];
                    [user_info setObject: person.image.url forKey:@"image"];
                    [user_info setObject:person.identifier forKey:@"id"];
                    //[user_info setObject:[GPPSignIn sharedInstance].userEmail forKey:@"userEmail"];
                    
                    if (self.delegate) {
                        [self.delegate googleGetUserInformationSucceeded:user_info];
                    }
                }
                else{
                    if (self.delegate) {
                        [self.delegate googleGetUserInformationFailedWithError:nil];
                    }
                }
            }
            
        }
    }
    @catch (NSException *exception) {
        
    }
}

#pragma -mark faceBook methods
-(void)faceBookLogin
{
    @try {
        if ([FBSession activeSession].state != FBSessionStateOpen &&
            [FBSession activeSession].state != FBSessionStateOpenTokenExtended)
        {
            [FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"email"]
                                               allowLoginUI:YES
                                          completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                              
                                              if (status == FBSessionStateOpen) {
                                                  [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error)
                                                   {
                                                       
                                                       if (!error) {
                                                           if (self.delegate) {
                                                               
                                                               NSMutableDictionary *user_info = [NSMutableDictionary new];
                                                               [user_info setObject:[user objectForKey:@"name"] forKey:@"name"];
                                                               [user_info setObject:[user objectForKey:@"id"] forKey:@"id"];
                                                               [user_info setObject:[[[FBSession activeSession] accessTokenData] accessToken] forKey:@"authToken"];
                                                               [user_info setObject:[user objectForKey:@"email"] forKey:@"email"];
                                                               [user_info setObject:[user objectForKey:@"gender"] forKey:@"gender"];
                                                               NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [user objectID]];
                                                               [user_info setObject:userImageURL    forKey:@"image"];
                                                               
                                                               [self.delegate faceBookGetUserInformationSucceeded:user_info];
                                                           }
                                                           
                                                       }
                                                       else{
                                                           if (self.delegate) {
                                                               [self.delegate faceBookGetUserInformationFailedWithError:error];
                                                           }
                                                       }
                                                       
                                                   }];
                                                  
                                              }
                                              else if (status == FBSessionStateClosed || status == FBSessionStateClosedLoginFailed){
                                                  if (self.delegate) {
                                                      [self.delegate faceBookAuthenticationFailedWithError:error];
                                                  }
                                              }
                                              
                                              
                                              
                                          }];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
@end
