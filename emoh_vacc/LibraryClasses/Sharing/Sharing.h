//
//  Sharing.h
//  emoh_vacc
//
//  Created by Assem Imam on 12/25/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//
@protocol sharingDelegate <NSObject>

@optional
-(void)liveAuthenticationFailedWithError:(NSError *) error;
-(void)liveGetUserInformationFailedWithError:(NSError *)error;
-(void)liveGetUserInformationSucceeded:(id)user_info;
-(void)twitterAuthenticationFailedWithError:(NSError *) error;
-(void)twitterGetUserInformationFailedWithError:(NSError *)error;
-(void)twitterGetUserInformationSucceeded:(id)user_info;
-(void)googleAuthenticationFailedWithError:(NSError *) error;
-(void)googleGetUserInformationFailedWithError:(NSError *)error;
-(void)googleGetUserInformationSucceeded:(id)user_info;
-(void)faceBookAuthenticationFailedWithError:(NSError *) error;
-(void)faceBookGetUserInformationFailedWithError:(NSError *)error;
-(void)faceBookGetUserInformationSucceeded:(id)user_info;

-(void)hideFaceBookUserInfo:(BOOL)shouldHide;
-(void)handleFaceBookSessionStateChangeWithNotification:(NSNotification *)notification;


@end
#import <Foundation/Foundation.h>
#import "LiveSDK/LiveConnectClient.h"
#import <Social/Social.h>
#import <TwitterKit/TwitterKit.h>
#import <Fabric/Fabric.h>
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>
#import <FacebookSDK/FacebookSDK.h>
@interface Sharing : NSObject
@property(assign)id<sharingDelegate>delegate;
@property(nonatomic,retain) UIViewController*currentSharingViewController;
+(Sharing *)getObject;
-(void)liveLoginWithApplicationClientID:(NSString*)client_id;
-(void)twitterLoginWithCustomerKey:(NSString*)customer_key CustomerSecret:(NSString*)customer_secret;
-(void)googleLoginWithClientID:(NSString*)client_id;
-(void)faceBookLogin;
@end
