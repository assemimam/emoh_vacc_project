//
//  CommonMethods.m
//
//
//  Created by Assem Imam on 1/19/14.
//  Copyright (c) 2014 tawasol. All rights reserved.
//

#import "CommonMethods.h"
#import <QuartzCore/QuartzCore.h>

@implementation CommonMethods
+(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+(BOOL) ValidateMail:(NSString *)mail
{
    mail = [mail lowercaseString];
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:mail];
}
+(void)FormatView:(UIView*)view WithShadowRadius:(float)shadow_radius CornerRadius:(float)Corner_radius ShadowOpacity:(float)opcaity BorderWidth:(int)border_width BorderColor:(UIColor*)border_color ShadowColor:(UIColor*)shadow_color andXPosition:(float)xPosition
{
    view.layer.cornerRadius = Corner_radius;
    view.layer.borderColor = border_color.CGColor;
    view.layer.borderWidth = border_width;
    view.layer.shadowColor = shadow_color.CGColor;
    view.layer.shadowOpacity = opcaity;
    view.layer.shadowRadius =shadow_radius;
    view.layer.shadowOffset = CGSizeMake(xPosition, 0);
}
+(void)FormatView:(UIView*)view
{
    view.layer.cornerRadius = 3;
    view.layer.borderColor = [UIColor blackColor].CGColor;
    view.layer.borderWidth = 0;
    view.layer.shadowColor = [UIColor blackColor].CGColor;
    view.layer.shadowOpacity = 0.8;
    view.layer.shadowRadius = 2.0;
    view.layer.shadowOffset = CGSizeMake(0, 0);

}
+(int)GetViewController:(id)viewController FromNavigationController:(UINavigationController*)nav_controller
{
    int index= 0;
    int retIndex=-1;
    @try {
       
        for (id controller in [nav_controller viewControllers]) {
            
            if ([controller class]== [viewController class]) {
                retIndex = index;
                break;
            }
            index++;
        }
        
    }
    @catch (NSException *exception) {
        
    }
    
    return retIndex;
}

+ (NSString *)GetHTML_Text:(NSString *)html {
    
    NSScanner *theScanner;
    NSString *text = nil;
    theScanner = [NSScanner scannerWithString:html];
    
    while ([theScanner isAtEnd] == NO) {
        
        [theScanner scanUpToString:@"<" intoString:NULL] ;
        
        [theScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return [[html  stringByReplacingOccurrencesOfString:@"   " withString:@" "] stringByReplacingOccurrencesOfString:@"  " withString:@" "];
}
+(UIColor *)GetColorFromHexaValue:(int)value
{
    return [UIColor colorWithRed:((float)((value & 0xFF0000) >> 16))/255.0 green:((float)((value & 0xFF00) >> 8))/255.0 blue:((float)(value & 0xFF))/255.0 alpha:1.0];
}
+(NSString *)encodeUrlWithString:(NSString *)urlString
{
    @try {
        if (urlString==Nil) {
            return @"";
        }
        urlString=[[[[[[urlString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]stringByReplacingOccurrencesOfString:@"{" withString:@"%7B"]stringByReplacingOccurrencesOfString:@"}" withString:@"%7D"]stringByReplacingOccurrencesOfString:@" " withString:@"%20"]stringByReplacingOccurrencesOfString:@"\\" withString:@"/"]stringByReplacingOccurrencesOfString:@"\"" withString:@"'"];
        
        
        //NSLog(@"url %@",urlString);
        return urlString;
    }
    @catch (NSException *exception) {
        
    }
    
}
+(BOOL)CheckPhoneNumber:(NSString *)number
{
    BOOL SUCCESS=YES;
    number = [number stringByReplacingOccurrencesOfString:@" " withString:@""];
    @try {
        if (![[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",number]]])
        {
            SUCCESS=NO;
        }
  
    }
    @catch (NSException *exception) {
        SUCCESS=NO;

    }
    return SUCCESS;
}
+ (NSString *)urlencode:(NSString*)url {
    NSMutableString *output = [NSMutableString string];
    const unsigned char *source = (const unsigned char *)[url UTF8String];
    int sourceLen = strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

+(NSString*) convertDateToString:(NSDate*) date withFormat:(NSString *)format
{
    @synchronized(self)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:format];
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        NSString* formattedDate= [dateFormatter stringFromDate:date];
        return formattedDate;
    }
}
+(void)roundImgView:(UIImageView *)imageView withCornerRadius:(float)cornerRadius
{
    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = cornerRadius;
    imageView.layer.borderColor = [UIColor whiteColor].CGColor;
    imageView.layer.borderWidth = 1.0;
}

+(bool)isValidString:(NSString *)string
{
    if (!string)
        return NO;
    if ([[string stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:@""])
        return NO;
    return YES;
}

+(NSString *)trimString:(NSString *)myString
{
    
    NSRange range = [myString rangeOfString:@"T"];
    NSString *newString = [myString substringToIndex:range.location];
    
    return newString;
}


@end
