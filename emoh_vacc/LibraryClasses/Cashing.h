//
//  Cashing.h
//  AmChamb
//
//  Created by Assem Imam on 3/11/14.
//  Copyright (c) 2014 tawasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DB_Recored.h"

@interface Cashing : NSObject
@property (nonatomic, retain) NSString *databaseName;
+ (Cashing *)getObject;
- (void)setDatabase:(NSString *)name;
-(BOOL)DeleteDataFromTable:(NSString*)table_name WhereCondition:(NSString*)where_condition;
-(BOOL)CashData:(id)data inTable:(NSString*)table_name;
-(NSMutableArray*)getDataFromCashWithSelectColumns:(id)selectColumns Tables:(NSArray*)table_list Where:(NSString*)where_condition FromIndex:(NSString*)fromIndex ToIndex:(NSString*)toIndex OrderByField:(NSString*)OrderField AscendingOrder:(BOOL)Order;
@end
