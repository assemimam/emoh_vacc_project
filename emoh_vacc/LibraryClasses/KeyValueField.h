//
//  KeyValueField.h
//  emoh_vacc
//
//  Created by iOS Developer on 12/28/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyValueField : NSObject

@property (nonatomic ,strong) id key;
@property (nonatomic ,strong) id value;

-(instancetype)initWithKey:(id)key
             Value:(id)value;

@end
