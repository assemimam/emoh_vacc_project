//
//  KeyValueField.m
//  emoh_vacc
//
//  Created by iOS Developer on 12/28/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "KeyValueField.h"

@implementation KeyValueField

-(instancetype)initWithKey:(id)key Value:(id)value
{
    self = [super init];
    if (self)
    {
        self.key = key;
        self.value = value;
    }
    
    return self;
}

@end
