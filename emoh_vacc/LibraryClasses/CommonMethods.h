//
//  CommonMethods.h
//
//
//  Created by Assem Imam on 1/19/14.
//  Copyright (c) 2014 tawasol. All rights reserved.
//
#import <Foundation/Foundation.h>


@interface CommonMethods : NSObject
+(UIColor *)colorFromHexString:(NSString *)hexString;
+(BOOL) ValidateMail:(NSString *)mail;
+(void)FormatView:(UIView*)view WithShadowRadius:(float)shadow_radius CornerRadius:(float)Corner_radius ShadowOpacity:(float)opcaity BorderWidth:(int)border_width BorderColor:(UIColor*)border_color ShadowColor:(UIColor*)shadow_color andXPosition:(float)xPosition;
+(void)FormatView:(UIView*)view;
+(NSString *)GetHTML_Text:(NSString *)html;
+(int)GetViewController:(id)viewController FromNavigationController:(UINavigationController*)nav_controller;
+(UIColor *)GetColorFromHexaValue:(int)value;
+(NSString*)encodeUrlWithString:(NSString*)urlString;
+(BOOL)CheckPhoneNumber:(NSString*)number;
+ (NSString *)urlencode:(NSString*)url ;
+(NSString*) convertDateToString:(NSDate*) date withFormat:(NSString *)format;
+(void)roundImgView:(UIImageView *)imageView withCornerRadius:(float)cornerRadius;
+(bool)isValidString:(NSString *)string;
+(NSString *)trimString:(NSString *)myString;
@end
