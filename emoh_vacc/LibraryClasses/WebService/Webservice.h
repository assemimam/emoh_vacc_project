//
//  Webservice.h
//  KFCA
//
//  Created by Assem Imam on 9/4/13.
//  Copyright (c) 2013 assem. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PRODUCTION_VERSION

#ifdef PRODUCTION_VERSION
#define BASE_URL @"http://emohvacc.azurewebsites.net/api/"
#else
#define BASE_URL @"http://emohvacc.azurewebsites.net/api/"
#endif

@interface Webservice : NSObject
{
    
}
+(id)loginWithUserName:(NSString*)user_name AccountToken:(NSString*)account_token PushNotification:(NSString*)push_notification AlertPeriodByDays:(NSString*)alert_period MobileNumber:(NSString*)mobile_number Name:(NSString*)name;

+(id)addNewChildWithUserId:(int)userId ChildName:(NSString *)childName Gender:(int)gender birthDate:(NSString *)birthDate Photo:(NSString *)photo;

+(id)getEncodedImgWithChildName:(NSString *)childName UserName:(NSString *)userName ImageString:(NSString *)imageString UserId:(int)userId;
+(id)getChildrenListByUserID:(NSString*)user_id;
+(id)getVaccinationOfSpecificChild:(int)childId andUserId:(int)userId andUpdatedDate:(NSString *)updateDate;
+(id)deleteChildWithChildID:(NSString*)child_id UserID:(NSString*)user_id;
@end
