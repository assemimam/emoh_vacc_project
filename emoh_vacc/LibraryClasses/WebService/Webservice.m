//
//  Webservice.m
//  KFCA
//
//  Created by Assem Imam on 9/4/13.
//  Copyright (c) 2013 assem. All rights reserved.
//

#import "Webservice.h"


@implementation Webservice
+(id)loginWithUserName:(NSString *)user_name AccountToken:(NSString *)account_token PushNotification:(NSString *)push_notification AlertPeriodByDays:(NSString *)alert_period MobileNumber:(NSString *)mobile_number Name:(NSString *)name
{
    id result;
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@AddUser", BASE_URL]];
        
        NSArray * keys = [NSArray arrayWithObjects:@"UserName",@"AccountToken",@"PushNotification",@"AlertPeriodByDays",@"MobileNumber",@"Name",nil];
        
        NSArray * values = [NSArray arrayWithObjects:user_name,account_token,push_notification,alert_period,mobile_number,name, nil];
        
        NSDictionary * params = [NSDictionary dictionaryWithObjects:values forKeys:keys];
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONReadingMutableContainers error:nil];
        NSString *jsonRequest = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"jsonRequest is %@", jsonRequest);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:160.0];
        
        NSData *requestData = [jsonRequest dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: requestData];
        
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        if([data length] == 0) return nil;
        
        NSString *incomingResultData =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;;
    }
    @catch (NSException *exception)
    {
        
    }
    return result;
}

//+(id)getAppointements
//{
//    @try {
//        id result;
//        @try {
//            NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@appointments", BASE_URL]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
//                                                            cachePolicy:NSURLCacheStorageAllowed
//                                                        timeoutInterval:120];
//            NSURLResponse * response = nil;
//            NSError * error = nil;
//            NSData * data = [NSURLConnection sendSynchronousRequest:request
//                                                  returningResponse:&response
//                                                              error:&error];
//            if (error == nil)
//            {
//                result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
//            }
//            else{
//                result=nil;
//            }
//        }
//        @catch (NSException *exception) {
//            
//        }
//        return result;
//    }
//    @catch (NSException *exception) {
//        
//    }
//}
+(id)addNewChildWithUserId:(int)userId ChildName:(NSString *)childName Gender:(int)gender birthDate:(NSString *)birthDate Photo:(NSString *)photo
{
    
    id result;
    if (photo==nil) {
        photo=@"";
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
   
    NSString *userToken =[[defaults objectForKey:@"user_info"]objectForKey:@"user_token"];
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@AddChild", BASE_URL]];
        
        NSArray * keys = [NSArray arrayWithObjects:@"UserId",@"ChildName",@"Gender",@"BirthDate",@"Photo",nil];
        
        NSArray * values = [NSArray arrayWithObjects:[NSNumber numberWithInt:userId],childName,[NSNumber numberWithInt:gender],birthDate,photo,nil];
        NSDictionary * params = [NSDictionary dictionaryWithObjects:values forKeys:keys];
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONReadingMutableContainers error:nil];
        NSString *jsonRequest = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"jsonRequest is %@", jsonRequest);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:160.0];
        
        NSData *requestData = [jsonRequest dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
        [request setValue:userToken forHTTPHeaderField:@"Authorization"];
        [request setHTTPBody: requestData];
        
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        if([data length] == 0) return nil;
        
        NSString *incomingResultData =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    }
    @catch (NSException *exception)
    {
        
    }
    return result;
    
}

+(id)getEncodedImgWithChildName:(NSString *)childName UserName:(NSString *)userName ImageString:(NSString *)imageString UserId:(int)userId
{
    id result;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userToken =[[defaults objectForKey:@"user_info"]objectForKey:@"user_token"];
  
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@UploadImage", BASE_URL]];
        
        NSArray * keys = [NSArray arrayWithObjects:@"ChildName",@"UserName",@"ImageAsString",@"UserId",nil];
        
        NSArray * values = [NSArray arrayWithObjects:childName,userName,imageString,[NSNumber numberWithInt:userId],nil];
        NSDictionary * params = [NSDictionary dictionaryWithObjects:values forKeys:keys];
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONReadingMutableContainers error:nil];
        NSString *jsonRequest = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        NSLog(@"jsonRequest is %@", jsonRequest);
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:160.0];
        
        NSData *requestData = [jsonRequest dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPMethod:@"POST"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
        [request setValue:userToken forHTTPHeaderField:@"Authorization"];
        [request setHTTPBody: requestData];
        
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        
        if([data length] == 0) return nil;
        
        NSString *incomingResultData =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;;
    }
    @catch (NSException *exception)
    {
        
    }
    return result;
}
+(id)getChildrenListByUserID:(NSString *)user_id
{
    id result;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userToken =[[defaults objectForKey:@"user_info"]objectForKey:@"user_token"];
    
    @try {
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@Children?userId=%@&lastUpdate=", BASE_URL,user_id]];
    
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:160.0];
        
        [request setValue:userToken forHTTPHeaderField:@"Authorization"];
        NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        if([data length] == 0) return nil;
        
        NSString *incomingResultData =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;;
    }
    @catch (NSException *exception)
    {
        
    }
    return result;
}


+(id)getVaccinationOfSpecificChild:(int)childId andUserId:(int)userId andUpdatedDate:(NSString *)updateDate
{
    @try {
        id result;
        @try {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSString *userToken =[[defaults objectForKey:@"user_info"]objectForKey:@"user_token"];

            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@VaccinationGroup?userId=%d&childId=%d&lastUpdate=",BASE_URL ,userId,childId]];
            
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                                   cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:160.0];

            
            [request setValue:userToken forHTTPHeaderField:@"Authorization"];
            
            NSURLResponse * response = nil;
            NSError * error = nil;
            NSData * data = [NSURLConnection sendSynchronousRequest:request
                                                  returningResponse:&response
                                                              error:&error];
            if (error == nil)
            {
                result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] ;
            }
            else{
                result=nil;
            }
        }
        @catch (NSException *exception) {
            
        }
        return result;
    }
    @catch (NSException *exception) {
        
    }

}
+(id)deleteChildWithChildID:(NSString*)child_id UserID:(NSString*)user_id
{
    id result;
    @try {
        
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userToken =[[defaults objectForKey:@"user_info"]objectForKey:@"user_token"];
     NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@DeleteChild?userId=%@&childId=%@",BASE_URL ,user_id,child_id]];
        
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:160.0];
    
    [request setHTTPMethod:@"DELETE"];
    [request setValue:userToken forHTTPHeaderField:@"Authorization"];
  
    
    NSData *data = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    if([data length] == 0) return nil;
    
    NSString *incomingResultData =[[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    result= [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
}
@catch (NSException *exception)
{
    
}
return result;

}


@end
