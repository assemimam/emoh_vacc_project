//
//  LibrariesConstants.h
//  MoRe
//
//  Created by Ahmed Aly on 10/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//


#import "Language.h"
#import "AppDelegate.h"
#import "WebService.h"
#import "CommonMethods.h"
#import "ImageCache.h"
#import "Database.h"


// iOS  Support
#define IS_DEVICE_RUNNING_IOS_AND_ABOVE(ios) ([[[UIDevice currentDevice] systemVersion] compare:ios options:NSNumericSearch] != NSOrderedAscending)
// NetworkService
#define NETWORK_TIMEOUT 5
#define SET_BACKGROUND [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background.png"]]];\
#define SHOW_ALERT(alertText, okButton) {\
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertText message:nil delegate:self cancelButtonTitle:okButton otherButtonTitles:nil];\
[alert show];\
}

