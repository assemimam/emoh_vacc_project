//
//  Database.h
//  MoRe
//
//  Created by Ahmed Aly on 10/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#ifndef _H_Database
#define _H_Database

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "DB_Field.h"
@interface Database : NSObject {
	sqlite3 *sqlDatabase;
    sqlite3_stmt *sqlStatement;
	NSString *databaseName;
}

@property (nonatomic, retain) NSString *databaseName;
@property BOOL  IS_LANDPLAN;
// Initialization

+ (Database *)getObject;
- (void)setDatabase:(NSString *)name;

-(NSString*)CopyDataBaseFileToDocumentsDirectory;
-(BOOL)InsertValues:(NSArray*)data  InTable:(NSString*)TableName;
-(BOOL)DeleteFromTable:(NSString*)tableName  Where:(NSString*)WhereCondition;
-(BOOL)UpdateValues:(NSArray*)data  InTable:(NSString*)TableName  Where:(NSString*)WhereCondition;
- (NSMutableArray *)selectColumns:(NSArray *)columns fromTables:(NSArray*)tables where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending withLimit:(NSString *)limit andOffect:(NSString*)offect;

- (NSArray *)selectColumns:(NSArray *)columns fromTable:(NSString *)table where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending withLimit:(NSString *)limit  andOffect:(NSString*)offect ;



///Fieldvalue
- (NSDictionary *)selectColumnsDic:(NSArray *)columns fromTable:(NSString *)table where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending withLimit:(NSString *)limit ;
- (NSDictionary *)searchInColumnDic:(NSString *)columnName forString:(NSString *)searchText andSelectColumns:(NSArray *)columns fromTable:(NSString *)table where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending withLimit:(NSString *)limit;


// Select From Database

- (NSDictionary *)selectColumns2:(NSArray *)columns fromTable:(NSString *)table where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending withLimit:(NSString *)limit;
- (NSArray *)arraySelectColumns:(NSArray *)columns fromTable:(NSString *)table where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending andgroupBy:(NSString*)group  withLimit:(NSString *)limit;

- (NSArray *)arraySelectColumns:(NSArray *)columns fromTable:(NSString *)table where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending withLimit:(NSString *)limit;

@end

#endif