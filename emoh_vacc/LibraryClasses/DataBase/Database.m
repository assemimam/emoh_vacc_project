//
//  Database.m
//  MoRe
//
//  Created by Ahmed Aly on 10/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "Database.h"
#import "DB_Field.h"

Database *userDatabaseObject = nil;

@implementation Database

@synthesize databaseName;
@synthesize IS_LANDPLAN;

#pragma -
#pragma Initialization

+ (Database *)getObject{
	if (userDatabaseObject == nil) {
		userDatabaseObject = [[Database alloc] init];
	}
	
	return userDatabaseObject;
}

- (void)setDatabase:(NSString *)name{
	self.databaseName = name;
	sqlDatabase = nil;
	sqlStatement = nil;
}

#pragma -
#pragma  Select From Database

/////
- (NSDictionary *)selectColumns2:(NSArray *)columns fromTable:(NSString *)table where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending withLimit:(NSString *)limit{
    //	NSString *databasePath = [[NSBundle mainBundle] pathForResource:self.databaseName ofType:@"db"];
    //	const char *cDatabasePath = [databasePath cStringUsingEncoding:NSUTF8StringEncoding];
    const char *cDatabasePath =  [[self CopyDataBaseFileToDocumentsDirectory ] UTF8String];
	sqlite3_open(cDatabasePath, &sqlDatabase);
	
	NSMutableDictionary *returnDictionary = [NSMutableDictionary dictionary];
	
	if (sqlDatabase != nil) {
		NSString *selectColumnsString = @"";
		NSString *whereString = @"";
		NSString *orderByString = @"";
		NSString *limitString = @"";
		
		for (int i=0; i<[columns count]; i++) {
			[returnDictionary setObject:[NSArray array] forKey:[columns objectAtIndex:i]];
			selectColumnsString = [selectColumnsString stringByAppendingFormat:@",%@",[columns objectAtIndex:i]];
		}
		if ([selectColumnsString length] > 0) {
			selectColumnsString = [selectColumnsString substringFromIndex:1];
		}
		
		if (whereConditions != nil) {
			whereString = [NSString stringWithFormat:@"WHERE %@",whereConditions];
		}
		
		if (order != nil) {
			if (ascending) {
				orderByString = [NSString stringWithFormat:@"ORDER BY %@ ASC",order];
			} else {
				orderByString = [NSString stringWithFormat:@"ORDER BY %@ DESC",order];
			}
		}
		
		if (limit != nil) {
			limitString = [NSString stringWithFormat:@"LIMIT %@",limit];
		}
		
		
		NSString *selectQuery = [NSString stringWithFormat:@"SELECT %@ FROM %@ %@ %@ %@",selectColumnsString, table, whereString, orderByString, limitString];
		const char *constQuery = [selectQuery UTF8String];
		if (sqlite3_prepare_v2(sqlDatabase, constQuery, -1, &sqlStatement, NULL) == SQLITE_OK) {
			while (sqlite3_step(sqlStatement) == SQLITE_ROW) {
				for (int i=0; i<[columns count]; i++) {
					NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
					NSString *columnValue = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(sqlStatement, i)];
					NSArray *columnArray = [returnDictionary objectForKey:[columns objectAtIndex:i]];
					columnArray = [columnArray arrayByAddingObject:columnValue];
					[columnValue release];
					[returnDictionary setObject:columnArray forKey:[columns objectAtIndex:i]];
					[pool release];
				}
			}
		}
	}
	
	sqlite3_finalize(sqlStatement);
	sqlite3_close(sqlDatabase);
	return [NSDictionary dictionaryWithDictionary:returnDictionary];
}


- (NSArray *)arraySelectColumns:(NSArray *)columns fromTable:(NSString *)table where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending andgroupBy:(NSString*)group  withLimit:(NSString *)limit
{
    
    const char *cDatabasePath =  [[self CopyDataBaseFileToDocumentsDirectory ] UTF8String];
	sqlite3_open(cDatabasePath, &sqlDatabase);
	
	NSMutableArray *returnArray = [NSMutableArray array];
	
	if (sqlDatabase != nil) {
		NSString *selectColumnsString = @"";
		NSString *whereString = @"";
		NSString *orderByString = @"";
        NSString *groupString = @"";
		NSString *limitString = @"";
        NSString *selectQuery=@"";
		
		for (int i=0; i<[columns count]; i++) {
			//[returnArray addObject:[NSDictionary dictionary]];
			selectColumnsString = [selectColumnsString stringByAppendingFormat:@",%@",[columns objectAtIndex:i]];
		}
		if ([selectColumnsString length] > 0) {
			selectColumnsString = [selectColumnsString substringFromIndex:1];
		}
		
		if (whereConditions != nil) {
			whereString = [NSString stringWithFormat:@"WHERE %@",whereConditions];
		}
		
		if (order != nil) {
			if (ascending) {
				orderByString = [NSString stringWithFormat:@"ORDER BY %@ ASC",order];
			} else {
				orderByString = [NSString stringWithFormat:@"ORDER BY %@ DESC",order];
			}
		}
		
		if (limit != nil) {
			limitString = [NSString stringWithFormat:@"LIMIT %@",limit];
		}
        
        if(group!=nil)
        {
            groupString=[NSString stringWithFormat:@"GROUP BY %@",group];
            selectQuery = [NSString stringWithFormat:@"SELECT %@ FROM %@ %@ %@ %@ %@",selectColumnsString, table, whereString,groupString,orderByString, limitString];
        }
		else {
            selectQuery = [NSString stringWithFormat:@"SELECT %@ FROM %@ %@ %@ %@",selectColumnsString, table, whereString,orderByString, limitString];
        }
		
		
        
        
        ////NSLog(@"Query=%@",selectQuery);
		const char *constQuery = [selectQuery UTF8String];
		if (sqlite3_prepare_v2(sqlDatabase, constQuery, -1, &sqlStatement, NULL) == SQLITE_OK) {
			while (sqlite3_step(sqlStatement) == SQLITE_ROW) {
                NSMutableDictionary *currentDictionary=[NSMutableDictionary dictionary];
				for (int i=0; i<[columns count]; i++) {
					NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
                    NSString *columnValue = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(sqlStatement, i)];
                    
                    NSArray *columnsKey = [[columns objectAtIndex:i] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
                    // //////NSLog(@"%@",columnsKey);
                    if ([columnsKey count]>1)
                        [currentDictionary setObject:columnValue forKey:[columnsKey objectAtIndex:1]];
                    else
                        [currentDictionary setObject:columnValue forKey:[columnsKey objectAtIndex:0]];
                    [columnValue release];
                    [pool release];
				}
                [returnArray addObject:currentDictionary];
			}
		}
	}
	
	sqlite3_finalize(sqlStatement);
	sqlite3_close(sqlDatabase);
	
	return [NSArray arrayWithArray:returnArray];
}


- (NSArray *)arraySelectColumns:(NSArray *)columns fromTable:(NSString *)table where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending withLimit:(NSString *)limit{
    if (!self.IS_LANDPLAN) {
        NSString *databasePath = [[NSBundle mainBundle] pathForResource:self.databaseName ofType:@"db"];
        const char *cDatabasePath = [databasePath cStringUsingEncoding:NSUTF8StringEncoding];
        sqlite3_open(cDatabasePath, &sqlDatabase);
    }
    else {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [paths objectAtIndex:0];
        path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite" ,databaseName]];
        
        
        const char *cDatabasePath = [path cStringUsingEncoding:NSUTF8StringEncoding];
        sqlite3_open(cDatabasePath, &sqlDatabase);
        
    }
    
  	
	NSMutableArray *returnArray = [NSMutableArray array];
	
	if (sqlDatabase != nil) {
		NSString *selectColumnsString = @"";
		NSString *whereString = @"";
		NSString *orderByString = @"";
		NSString *limitString = @"";
		
		for (int i=0; i<[columns count]; i++) {
			selectColumnsString = [selectColumnsString stringByAppendingFormat:@",%@",[columns objectAtIndex:i]];
		}
		if ([selectColumnsString length] > 0) {
			selectColumnsString = [selectColumnsString substringFromIndex:1];
		}
		
		if (whereConditions != nil) {
			whereString = [NSString stringWithFormat:@"WHERE %@",whereConditions];
		}
		
		if (order != nil) {
			if (ascending) {
				orderByString = [NSString stringWithFormat:@"ORDER BY %@ ASC",order];
			} else {
				orderByString = [NSString stringWithFormat:@"ORDER BY %@ DESC",order];
			}
		}
		
		if (limit != nil) {
			limitString = [NSString stringWithFormat:@"LIMIT %@",limit];
		}
		
		
		NSString *selectQuery = [NSString stringWithFormat:@"SELECT %@ FROM %@ %@ %@ %@",selectColumnsString, table, whereString, orderByString, limitString];
        ////////NSLog(@"Slect %@",selectQuery);
		const char *constQuery = [selectQuery UTF8String];
		if (sqlite3_prepare_v2(sqlDatabase, constQuery, -1, &sqlStatement, NULL) == SQLITE_OK) {
			while (sqlite3_step(sqlStatement) == SQLITE_ROW) {
				NSMutableDictionary *rowData = [[NSMutableDictionary alloc] init];
				for (int i=0; i<[columns count]; i++) {
                    const unsigned char *temp = sqlite3_column_text(sqlStatement, i);
                    if(temp  != nil){
                        
                        NSString *columnValue = [[NSString alloc] initWithUTF8String:(const char *)temp];
                        [rowData setObject:columnValue forKey:[columns objectAtIndex:i]];
                        [columnValue release];
                    }
				}
				[returnArray addObject:rowData];
				[rowData release];
			}
		}
	}
	
	sqlite3_finalize(sqlStatement);
	sqlite3_close(sqlDatabase);
	
	return [NSArray arrayWithArray:returnArray];
}


- (NSDictionary *)selectColumnsDic:(NSArray *)columns fromTable:(NSString *)table where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending withLimit:(NSString *)limit{
	NSString *databasePath = [[NSBundle mainBundle] pathForResource:self.databaseName ofType:@"sqlite"];
	const char *cDatabasePath = [databasePath cStringUsingEncoding:NSUTF8StringEncoding];
    
	sqlite3_open(cDatabasePath, &sqlDatabase);
	
	NSMutableDictionary *returnDictionary = [NSMutableDictionary dictionary];
	
	if (sqlDatabase != nil) {
		NSString *selectColumnsString = @"";
		NSString *whereString = @"";
		NSString *orderByString = @"";
		NSString *limitString = @"";
		
		for (int i=0; i<[columns count]; i++) {
			[returnDictionary setObject:[NSArray array] forKey:[columns objectAtIndex:i]];
			selectColumnsString = [selectColumnsString stringByAppendingFormat:@",%@",[columns objectAtIndex:i]];
		}
		if ([selectColumnsString length] > 0) {
			selectColumnsString = [selectColumnsString substringFromIndex:1];
		}
		
		if (whereConditions != nil) {
			whereString = [NSString stringWithFormat:@"WHERE %@",whereConditions];
		}
		
		if (order != nil) {
			if (ascending) {
				orderByString = [NSString stringWithFormat:@"ORDER BY %@ ASC",order];
			} else {
				orderByString = [NSString stringWithFormat:@"ORDER BY %@ DESC",order];
			}
		}
		
		if (limit != nil) {
			limitString = [NSString stringWithFormat:@"LIMIT %@",limit];
		}
		
		
		NSString *selectQuery = [NSString stringWithFormat:@"SELECT %@ FROM %@ %@ %@ %@",selectColumnsString, table, whereString, orderByString, limitString];
		const char *constQuery = [selectQuery UTF8String];
		if (sqlite3_prepare_v2(sqlDatabase, constQuery, -1, &sqlStatement, NULL) == SQLITE_OK) {
			while (sqlite3_step(sqlStatement) == SQLITE_ROW) {
				for (int i=0; i<[columns count]; i++) {
					NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
					NSString *columnValue = [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(sqlStatement, i)];
					NSArray *columnArray = [returnDictionary objectForKey:[columns objectAtIndex:i]];
					columnArray = [columnArray arrayByAddingObject:columnValue];
					[columnValue release];
					[returnDictionary setObject:columnArray forKey:[columns objectAtIndex:i]];
					[pool release];
				}
			}
		}
	}
	
	sqlite3_finalize(sqlStatement);
	sqlite3_close(sqlDatabase);
	
	return [NSDictionary dictionaryWithDictionary:returnDictionary];
}






- (NSDictionary *)searchInColumnDic:(NSString *)columnName forString:(NSString *)searchText andSelectColumns:(NSArray *)columns fromTable:(NSString *)table where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending withLimit:(NSString *)limit{
	NSString *databasePath = [[NSBundle mainBundle] pathForResource:self.databaseName ofType:@"sqlite"];
	const char *cDatabasePath = [databasePath cStringUsingEncoding:NSUTF8StringEncoding];
	sqlite3_open(cDatabasePath, &sqlDatabase);
	
	NSMutableDictionary *returnDictionary = [NSMutableDictionary dictionary];
	
	if (sqlDatabase != nil) {
		NSString *selectColumnsString = @"";
		NSString *whereString = @"";
		NSString *orderByString = @"";
		NSString *limitString = @"";
		NSMutableArray *columnsNames = [NSMutableArray array];
		for (int i=0; i<[columns count]; i++) {
            
            NSString* columnName = [[columns objectAtIndex:i] substringFromIndex:[[columns objectAtIndex:i] rangeOfString:@"."].location+1];
          	[columnsNames addObject:columnName];
            [returnDictionary setObject:[NSArray array] forKey:columnName];
			selectColumnsString = [selectColumnsString stringByAppendingFormat:@",%@",[columns objectAtIndex:i]];
		}
		if ([selectColumnsString length] > 0) {
			selectColumnsString = [selectColumnsString substringFromIndex:1];
		}
		
		if (whereConditions == nil) {
			whereString = [NSString stringWithFormat:@"WHERE %@ LIKE '%%%@%%'",columnName,searchText];
		} else {
			whereString = [NSString stringWithFormat:@"WHERE %@ AND %@ LIKE '%%%@%%'",whereConditions,columnName,searchText];
		}
		
		if (order != nil) {
			if (ascending) {
				orderByString = [NSString stringWithFormat:@"ORDER BY %@ ASC",order];
			} else {
				orderByString = [NSString stringWithFormat:@"ORDER BY %@ DESC",order];
			}
		}
		
		if (limit != nil) {
			limitString = [NSString stringWithFormat:@"LIMIT %@",limit];
		}
		
		
		NSString *selectQuery = [NSString stringWithFormat:@"SELECT %@ FROM %@ %@ %@ %@",selectColumnsString, table, whereString, orderByString, limitString];
		const char *constQuery = [selectQuery UTF8String];
		if (sqlite3_prepare_v2(sqlDatabase, constQuery, -1, &sqlStatement, NULL) == SQLITE_OK) {
			while (sqlite3_step(sqlStatement) == SQLITE_ROW) {
				for (int i=0; i<[columns count]; i++) {
					NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
					NSString *columnValue = [NSString stringWithUTF8String:(const char *)sqlite3_column_text(sqlStatement, i)];
					NSArray *columnArray = [returnDictionary objectForKey:[columnsNames objectAtIndex:i]];
					columnArray = [columnArray arrayByAddingObject:columnValue];
					[returnDictionary setObject:columnArray forKey:[columnsNames objectAtIndex:i]];
					[pool release];
				}
			}
		}
	}
	
	sqlite3_finalize(sqlStatement);
	sqlite3_close(sqlDatabase);
	
	return [NSDictionary dictionaryWithDictionary:returnDictionary];
}


-(NSString*)CopyDataBaseFileToDocumentsDirectory
{
    if([databaseName isEqualToString:@"db"])
    {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *path = [paths objectAtIndex:0];
    return  [path stringByAppendingPathComponent:@"db.db"];
    }
    else {
        
    NSString *dbSourcePath = [[NSBundle mainBundle] pathForResource:self.databaseName ofType:@"db"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *Documentpath = [paths objectAtIndex:0];
    NSString *TargetPath = [Documentpath stringByAppendingPathComponent:[self.databaseName stringByAppendingPathExtension:@"db"]];
    
    NSFileManager *Fmanager = [NSFileManager defaultManager];
    if ([Fmanager fileExistsAtPath:TargetPath]) {
        
    }
    else {
        [Fmanager copyItemAtPath:dbSourcePath toPath:TargetPath error:nil];
    }
    
    return TargetPath;
    }
}
-(BOOL)DeleteFromTable:(NSString*)tableName  Where:(NSString*)WhereCondition
{
    @try {
        NSString *sqlInsertStatement=@"";
        if (WhereCondition !=nil) {
            if ([[WhereCondition stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]] length] == 0)
            {
                sqlInsertStatement=[NSString stringWithFormat:@"Delete From %@ ;",tableName];
            }
            else {
                sqlInsertStatement=[NSString stringWithFormat:@"Delete From %@ Where %@ ;",tableName,WhereCondition];
            }
        }
        // ////NSLog(sqlInsertStatement);
        const char *cDatabasePath =  [[self CopyDataBaseFileToDocumentsDirectory ] UTF8String];//[databasePath cStringUsingEncoding:
        sqlite3_open(cDatabasePath, &sqlDatabase);
        char *ErrorMessage;
        
        int returnCode = sqlite3_exec(sqlDatabase, [sqlInsertStatement  UTF8String], NULL, NULL, &ErrorMessage);
        if(returnCode!=SQLITE_OK) {
            fprintf(stderr, "Error in inserting to table. Error: %s", ErrorMessage);
            sqlite3_free(ErrorMessage);
            return NO;   
        }
        
        sqlite3_close(sqlDatabase);
         return  YES;
    }
    @catch (NSException *exception) {
        return  NO;
    }
   
    
   
    
}



-(BOOL)InsertValues:(NSArray*)data  InTable:(NSString*)TableName
{
    [self CopyDataBaseFileToDocumentsDirectory];
    sqlite3 *sqlDatabaseUpadte;
    if ([data count] > 0) {
        NSString *sqlInsertStatement=[NSString stringWithFormat:@"insert into %@(",TableName]; 
        
        for (DB_Field *column  in data) {
            sqlInsertStatement = [sqlInsertStatement stringByAppendingFormat:@"%@ ,",column.FieldName];
        }
        sqlInsertStatement = [sqlInsertStatement substringToIndex:sqlInsertStatement.length -1];
        sqlInsertStatement= [sqlInsertStatement stringByAppendingString:@") Values ("];
        
        int index=0;
        for (DB_Field *column in data) {
           
            switch (column.FieldDataType) {
                case FIELD_DATA_TYPE_NUMBER:
                   sqlInsertStatement = [sqlInsertStatement stringByAppendingFormat:@"%@ ,",column.FieldValue];  
                    break;
                case FIELD_DATA_TYPE_TEXT:
                    sqlInsertStatement = [sqlInsertStatement stringByAppendingFormat:@"'%@' ,",[column.FieldValue stringByReplacingOccurrencesOfString:@"'" withString:@"''"]];
                    break;
                case FIELD_DATA_TYPE_DATE:
                   sqlInsertStatement = [sqlInsertStatement stringByAppendingFormat:@"'%@' ,",column.FieldValue];
                    break;
                    case FIELD_DATA_TYPE_IMAGE:
                    case FIELD_DATA_TYPE_HTML_FILE:
                    break;
                
            }
           
            index++;
        }

        sqlInsertStatement = [sqlInsertStatement substringToIndex:sqlInsertStatement.length -1];
        sqlInsertStatement= [sqlInsertStatement stringByAppendingString:@");"];
        
        const char *cDatabasePath =  [[self CopyDataBaseFileToDocumentsDirectory ] UTF8String];//[databasePath cStringUsingEncoding:

       if (sqlite3_open(cDatabasePath, &sqlDatabaseUpadte)== SQLITE_OK) {
           char *ErrorMessage;
           
           
           //sqlite3_exec(sqlDatabase, "BEGIN TRANSACTION", NULL, NULL, &ErrorMessage);
//           if(sqlite3_prepare(sqlDatabase, [sqlInsertStatement  UTF8String], -1, &sqlStatement, NULL) == SQLITE_OK){
//               if(sqlite3_step(sqlStatement) == SQLITE_DONE)
//               {
//                   
//               }
//               else {
//                   //sqlite3_exec(sqlDatabase, "COMMIT TRANSACTION", NULL, NULL, &ErrorMessage);
//                   //NSLog(@"error insert %@",sqlInsertStatement);
//                   //NSLog(@"error: %s", sqlite3_errmsg(sqlDatabase));
//                   sqlite3_finalize(sqlStatement);
//                   sqlite3_close(sqlDatabase);
//                   return NO;
//                   
//               }
//           }
           
                   int returnCode = sqlite3_exec(sqlDatabaseUpadte, [sqlInsertStatement  UTF8String], NULL, NULL, &ErrorMessage);
                  if(returnCode!=SQLITE_OK) {
                       //NSLog(@"error insert %@",sqlInsertStatement);
                       fprintf(stderr, "Error in inserting to table. Error: %s", ErrorMessage);
                     sqlite3_free(ErrorMessage);
                      
                      sqlite3_close(sqlDatabaseUpadte);
                      return NO;
                   }
           
//           sqlite3_finalize(sqlStatement);
           sqlite3_close(sqlDatabaseUpadte);
     
       }
        
        
    }
    else {
        return NO;
    }
   

    return YES;
}

-(BOOL)UpdateValues:(NSArray*)data  InTable:(NSString*)TableName  Where:(NSString*)WhereCondition
{
    
    sqlite3 *sqlDatabaseUpadte;
   
    if ([data count] > 0) {
        NSString *sqlUpdateStatement=[NSString stringWithFormat:@"update %@ Set ",TableName];
        
        for (DB_Field *column  in data) {
            id value=@"";
            switch (column.FieldDataType) {
                case FIELD_DATA_TYPE_NUMBER:
                    value = [NSString stringWithFormat:@"%@",column.FieldValue];
                    break;
                case FIELD_DATA_TYPE_TEXT:
                    value = [NSString stringWithFormat:@"'%@'",[column.FieldValue stringByReplacingOccurrencesOfString:@"'" withString:@"''"]];
                    break;
                case FIELD_DATA_TYPE_DATE:
                    value = [NSString stringWithFormat:@"'%@'",column.FieldValue];
                    break;
               
                                        
            }
            
            sqlUpdateStatement = [sqlUpdateStatement stringByAppendingFormat:@"%@ = %@,",column.FieldName,value];
            
            
        }
        sqlUpdateStatement = [sqlUpdateStatement substringToIndex:sqlUpdateStatement.length -1];
        if (WhereCondition!=nil) {
             sqlUpdateStatement= [sqlUpdateStatement stringByAppendingFormat:@" where %@ ;",WhereCondition];
        }
        else
        {
              sqlUpdateStatement= [sqlUpdateStatement stringByAppendingString:@" ;"];
        }
      
        const char *cDatabasePath =  [[self CopyDataBaseFileToDocumentsDirectory ] UTF8String];//[databasePath cStringUsingEncoding:
      
        if (sqlite3_open(cDatabasePath, &sqlDatabaseUpadte)== SQLITE_OK) {
            char *ErrorMessage;
                    int returnCode = sqlite3_exec(sqlDatabaseUpadte, [sqlUpdateStatement  UTF8String], NULL, NULL, &ErrorMessage);
                   if(returnCode!=SQLITE_OK) {
                      fprintf(stderr, "Error in update to table. Error: %s", ErrorMessage);
                     sqlite3_free(ErrorMessage);
                      
                     return NO;
                  }
            
            sqlite3_close(sqlDatabaseUpadte);
 

        }

    }
    else {
        return NO;
    }
    
    
    return YES;
}



- (NSMutableArray *)selectColumns:(NSArray *)columns fromTables:(NSArray*)tables where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending withLimit:(NSString *)limit andOffect:(NSString*)offect

{
    //	NSString *databasePath = [[NSBundle mainBundle] pathForResource:self.databaseName ofType:@"db"];
    //	const char *cDatabasePath = [databasePath cStringUsingEncoding:NSUTF8StringEncoding];
    
    @try {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [paths objectAtIndex:0];
        path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.db" ,databaseName]];
        
        
        const char *cDatabasePath = [path cStringUsingEncoding:NSUTF8StringEncoding];
        sqlite3_open(cDatabasePath, &sqlDatabase);
        
        
        
        
        
        NSMutableArray *returnArray = [NSMutableArray array];
        
        if (sqlDatabase != nil) {
            NSString *selectColumnsString = @"";
            NSString *tablesString=@"";
            NSString *whereString = @"";
            NSString *orderByString = @"";
            NSString *limitString = @"";
            NSString *offectString = @"";
            for (DB_Field *column in columns) {
                NSString *column_name=@"";
                if (column.FieldAlias==nil) {
                    column_name= column.FieldName;
                }
                else{
                    column_name=[NSString stringWithFormat:@"%@ as '%@'",column.FieldName, column.FieldAlias];
                    
                }
                selectColumnsString = [selectColumnsString stringByAppendingFormat:@",%@",column_name];
            }
            
            for (int i=0; i<[columns count]; i++) {
                //[returnArray addObject:[NSDictionary dictionary]];
                
            }
            if ([selectColumnsString length] > 0) {
                selectColumnsString = [selectColumnsString substringFromIndex:1];
            }
            
            if (whereConditions != nil) {
                whereString = [NSString stringWithFormat:@"WHERE %@",whereConditions];
            }
            
            if (order != nil) {
                if (ascending) {
                    orderByString = [NSString stringWithFormat:@"ORDER BY %@ ASC ",order];
                } else {
                    orderByString = [NSString stringWithFormat:@"ORDER BY %@ DESC ",order];
                }
            }
            
            if (limit != nil) {
                limitString = [NSString stringWithFormat:@"LIMIT %@",limit];
            }
            if (offect!=nil) {
                offectString = [NSString stringWithFormat:@"OFFSET %@",offect];
            }
            if ([tables count]>0) {
                for (NSString *tableName in tables) {
                    tablesString = [tablesString stringByAppendingFormat:@"%@,",tableName];
                }
                tablesString =[tablesString substringToIndex:tablesString.length-1];
            }
            NSString *selectQuery = [NSString stringWithFormat:@"SELECT Distinct %@ FROM %@ %@ %@ %@ %@ ",selectColumnsString, tablesString, whereString, orderByString, limitString,offectString];
            ////NSLog(@"Query=%@",selectQuery);
            const char *constQuery = [selectQuery UTF8String];
            if (sqlite3_prepare_v2(sqlDatabase, constQuery, -1, &sqlStatement, NULL) == SQLITE_OK) {
                while (sqlite3_step(sqlStatement) == SQLITE_ROW) {
                    NSMutableDictionary *currentDictionary=[[[NSMutableDictionary alloc]init]autorelease];
                    for (int i=0; i<[columns count]; i++) {
                        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
                        NSString *columnValue=@"";
                        if (sqlite3_column_text(sqlStatement, i)==nil) {
                            
                        }
                        else
                        {
                            columnValue= [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(sqlStatement, i)];
                        }
                        DB_Field *currentColumn =[columns objectAtIndex:i];
                        NSArray *columnsKey=nil ;
                        if (currentColumn.FieldAlias==nil) {
                            columnsKey= [currentColumn.FieldName componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
                        }
                        else {
                            columnsKey= [NSArray arrayWithObject:currentColumn.FieldAlias];
                        }
                        // //////NSLog(@"%@",columnsKey);
                        if ([columnsKey count]>1)
                        [currentDictionary setObject:columnValue forKey:[columnsKey objectAtIndex:1]];
                        else
                        [currentDictionary setObject:columnValue forKey:[columnsKey objectAtIndex:0]];
                        
                        [columnValue release];
                        [pool release];
                    }
                    
                    
                    [returnArray addObject:currentDictionary];
                }
            }
        }
        
        sqlite3_finalize(sqlStatement);
        sqlite3_close(sqlDatabase);
        
        return [NSMutableArray arrayWithArray:returnArray];
    }
    @catch (NSException *exception) {
        
    }
    
    
   
}



- (NSArray *)selectColumns:(NSArray *)columns fromTable:(NSString *)table where:(NSString *)whereConditions andOrderBy:(NSString *)order inAscendingOrder:(BOOL)ascending withLimit:(NSString *)limit andOffect:(NSString*)offect

{
//	NSString *databasePath = [[NSBundle mainBundle] pathForResource:self.databaseName ofType:@"db"];
//	const char *cDatabasePath = [databasePath cStringUsingEncoding:NSUTF8StringEncoding];
    @try {
        sqlite3 *sqlDatabaseSelect;
        sqlite3_stmt *sqlStatementSelect;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [paths objectAtIndex:0];
        path = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.db" ,databaseName]];
        const char *cDatabasePath = [path cStringUsingEncoding:NSUTF8StringEncoding];
        
        
        if(sqlite3_open(cDatabasePath, &sqlDatabaseSelect) == SQLITE_OK)
        {
            NSMutableArray *returnArray = [NSMutableArray array];
            
            if (sqlDatabaseSelect != nil) {
                NSString *selectColumnsString = @"";
                NSString *whereString = @"";
                NSString *orderByString = @"";
                NSString *limitString = @"";
                NSString *offectString = @"";
                for (int i=0; i<[columns count]; i++) {
                    //[returnArray addObject:[NSDictionary dictionary]];
                    selectColumnsString = [selectColumnsString stringByAppendingFormat:@",%@",[columns objectAtIndex:i]];
                }
                if ([selectColumnsString length] > 0) {
                    selectColumnsString = [selectColumnsString substringFromIndex:1];
                }
                
                if (whereConditions != nil) {
                    whereString = [NSString stringWithFormat:@"WHERE %@",whereConditions];
                }
                
                if (order != nil) {
                    if (ascending) {
                        orderByString = [NSString stringWithFormat:@"ORDER BY %@ ASC ",order];
                    } else {
                        orderByString = [NSString stringWithFormat:@"ORDER BY %@ DESC ",order];
                    }
                }
                
                if (limit != nil) {
                    limitString = [NSString stringWithFormat:@"LIMIT %@",limit];
                }
                if (offect!=nil) {
                    offectString = [NSString stringWithFormat:@"OFFSET %@",offect];
                }
                
                
                NSString *selectQuery = [NSString stringWithFormat:@"SELECT Distinct %@ FROM %@ %@ %@ %@ %@ ",selectColumnsString, table, whereString, orderByString, limitString,offectString];
                ////NSLog(@"Query=%@",selectQuery);
                const char *constQuery = [selectQuery UTF8String];
                if (sqlite3_prepare_v2(sqlDatabaseSelect, constQuery, -1, &sqlStatementSelect, NULL) == SQLITE_OK) {
                    while (sqlite3_step(sqlStatementSelect) == SQLITE_ROW) {
                        NSMutableDictionary *currentDictionary=[NSMutableDictionary dictionary];
                        for (int i=0; i<[columns count]; i++) {
                            NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
                            NSString *columnValue=@"";
                            if (sqlite3_column_text(sqlStatementSelect, i)==nil) {
                                
                            }
                            else
                            {
                                columnValue= [[NSString alloc] initWithUTF8String:(const char *)sqlite3_column_text(sqlStatementSelect, i)];
                            }
                            NSArray *columnsKey = [[columns objectAtIndex:i] componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"."]];
                            // //////NSLog(@"%@",columnsKey);
                            if ([columnsKey count]>1)
                                [currentDictionary setObject:columnValue forKey:[columnsKey objectAtIndex:1]];
                            else
                                [currentDictionary setObject:columnValue forKey:[columnsKey objectAtIndex:0]];
                            [columnValue release];
                            [pool release];
                        }
                        [returnArray addObject:currentDictionary];
                    }
                }
            }
           
            if (sqlStatementSelect) {
                sqlite3_finalize(sqlStatementSelect);
               
            }
            if (sqlDatabaseSelect) {
                sqlite3_close(sqlDatabaseSelect);
            }
            
            
            return [NSArray arrayWithArray:returnArray];
        }
        

    }
    @catch (NSException *exception) {
        
    }
   
    
 }






@end
