//
//  ChildProfileViewController.h
//  emoh_vacc
//
//  Created by iOS Developer on 1/5/15.
//  Copyright (c) 2015 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILTranslucentView.h"
@interface ChildProfileViewController : UIViewController <UITableViewDataSource , UITableViewDelegate>
{
    __weak IBOutlet ILTranslucentView *HeaderView;
    __weak IBOutlet UITableView *ChildProfileTableView;
    __weak IBOutlet UILabel *childNameLabel;
}


-(IBAction)deleteChildButtonAction:(UIButton *)sender;
-(IBAction)backButtonAction:(UIButton *)sender;

@end
