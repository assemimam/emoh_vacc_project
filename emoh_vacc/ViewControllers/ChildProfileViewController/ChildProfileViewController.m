//
//  ChildProfileViewController.m
//  emoh_vacc
//
//  Created by iOS Developer on 1/5/15.
//  Copyright (c) 2015 Nuitex. All rights reserved.
//

#import "ChildProfileViewController.h"
#import "ChildProfileCell.h"
#import "UIScrollView+APParallaxHeader.h"

@interface ChildProfileViewController ()<APParallaxViewDelegate>
{
    NSMutableArray *childDataSource;
    NSString *childName;
    NSString *childBirthDate;
    NSString *childGender;
}
@end

@implementation ChildProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    HeaderView.translucentAlpha = 0.7;
    HeaderView.translucentStyle = UIBarStyleDefault;
    HeaderView.translucentTintColor = [UIColor colorWithRed:241/255.0f green:90/255.0f blue:36/255.0f alpha:1];
    HeaderView.backgroundColor = [UIColor clearColor];
    
    
    SHOW_LOADING
    [self performSelector:@selector(getChildVacc) withObject:nil];
    
    [ChildProfileTableView addParallaxWithImage:[UIImage imageNamed:@"ParallaxImage.jpg"] andHeight:192 andShadow:YES andChildGender:childGender andChildBirthDate:childBirthDate];
    
    ChildProfileTableView.parallaxView.delegate = self;
    
}


#pragma mark -  get data from WS
-(void)getChildVacc
{
    id result = [Webservice getVaccinationOfSpecificChild:194 andUserId:108 andUpdatedDate:nil];
    if ([[result objectForKey:@"IsSuccess"] intValue] == 1 )
    {
        childDataSource = [result objectForKey:@"VaccinationGroupResult"];
        
        for (NSDictionary *dic in childDataSource)
        {
            id vaccResult = dic[@"Vaccinations"][0];
            id childObj = vaccResult[@"Child"];
            childBirthDate = childObj[@"BirthDateFormatted"];
            childName = childObj[@"ChildName"];
            int gender = [childObj[@"Gender"] intValue];
            if (gender == 0)
            {
                childGender = @"ذكر";
            }
            else if (gender == 1)
            {
                 childGender = @"أنثي";
            }
            childNameLabel.text = childName;
        }

        [ChildProfileTableView reloadData];
        
        HIDE_LOADING
    }
    else
    {
        
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [childDataSource count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifer = @"cell";
    ChildProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifer];
    if (!cell)
    {
        cell = [ChildProfileCell setUpChildProfileCell];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    id result = childDataSource[indexPath.row];
    
    
    NSString *VaccGroupName = [result objectForKey:@"VaccinationGroup"][@"VaccinationGroupArabicName"];
    NSString *VaccDueDate =  [CommonMethods  trimString:[result objectForKey:@"Vaccinations"][0][@"VaccinationDueDate"]];
    int VaccStatus =[[ result objectForKey:@"Vaccinations"][0][@"VaccinationStatus"] intValue];
    
    cell.VaccTypeLabel.text =  VaccGroupName;
    cell.VaccDateLabel.text =  VaccDueDate;
    
    NSDate *vaccDate =[self convertStringToDate:VaccDueDate withFormat:@"yyyy-mm-dd"];
    
    if (VaccStatus  == 0)
    {
        if ([[NSDate date] compare:vaccDate]==NSOrderedDescending)
        {
            cell.VaccImageView.image =  [UIImage imageNamed:@"Injection.png"];
        }
        else if([[NSDate date] compare:vaccDate]==NSOrderedAscending)
        {
            cell.VaccImageView.image =  [UIImage imageNamed:@"injectionrednameic"];
        }
        
    }
    else if (VaccStatus == 1)
    {
        cell.VaccImageView.image =  [UIImage imageNamed:@"injectiongreennameic.png"];
        
    }
    
    return cell;
}


#pragma mark - APParallaxViewDelegate

- (void)parallaxView:(APParallaxView *)view willChangeFrame:(CGRect)frame {
    // Do whatever you need to do to the parallaxView or your subview before its frame changes
}

- (void)parallaxView:(APParallaxView *)view didChangeFrame:(CGRect)frame {
    // Do whatever you need to do to the parallaxView or your subview after its frame changed
}


#pragma mark -  actions

-(IBAction)deleteChildButtonAction:(UIButton *)sender
{
    
}

-(IBAction)backButtonAction:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(NSDate*) convertStringToDate:(NSString*)dateString withFormat:(NSString *)format
{
    @synchronized(self)
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:format];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:dateString];
        return dateFromString;
    }
}

@end
