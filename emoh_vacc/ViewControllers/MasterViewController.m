//
//  MasterViewController.m
//  emoh_vacc
//
//  Created by Assem Imam on 12/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "MasterViewController.h"
#import "LibrariesHeader.h"

@interface MasterViewController ()

@end

@implementation MasterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    self.navigationController.navigationBarHidden=YES;
    [self.menuContainerViewController setPaningEnabled:YES];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(menuStateEventOccurred:)
                                                 name:MFSideMenuStateNotificationEvent
                                               object:nil];
   
}
- (void)menuStateEventOccurred:(NSNotification *)notification {
    MFSideMenuStateEvent event = [[[notification userInfo] objectForKey:@"eventType"] intValue];
    if (event == MFSideMenuStateEventMenuWillOpen) {
        HIDE_LOADING;
    }
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)OpenSideMenuAction:(UIButton *)sender {
    @try {
         [self.menuContainerViewController setMenuState:MFSideMenuStateLeftMenuOpen];
    }
    @catch (NSException *exception) {
        
    }
  
}
@end
