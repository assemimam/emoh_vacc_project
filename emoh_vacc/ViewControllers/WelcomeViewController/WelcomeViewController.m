//
//  WelcomeViewController.m
//  emoh_vacc
//
//  Created by Assem Imam on 12/30/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "WelcomeViewController.h"
#import "AddChildViewController.h"
#import "StartView.h"
@interface WelcomeViewController ()<UIScrollViewDelegate>
{
    StartView *addedView;
    NSString *btnTag;
}
@end

@implementation WelcomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"patternws.png"]];
    [self setUpPaging];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - scroll View Delegate



- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)setUpPaging
{
    [[UIApplication sharedApplication]setStatusBarHidden:YES];
    for (int i = 0; i < 3; i++)
    {
        addedView = [StartView setupStartView];
        [addedView setFrame:CGRectMake(i * SplashScrollView.frame.size.width, 0, SplashScrollView.frame.size.width, SplashScrollView.frame.size.height)];
        
        switch (i)
        {
            case 0:
            {
                addedView.img.image = [UIImage imageNamed:@"boyws.png"];
                [addedView.lbl setText:@"إبدأ بإضافة أطفالك الأن"];
                [AddNewChildButton setHidden:NO];
                [SplashScrollView setContentOffset:CGPointMake(3 * addedView.frame.size.width, 0)];
            }
                break;
            case 1:
            {
                addedView.img.image = [UIImage imageNamed:@"ringws.png"];
                [AddNewChildButton setHidden:YES];
                [addedView.lbl setText:@"احصل علي تنبيهات التطعيمات أولاً بأول"];
            }
                break;
            case 2:
            {
                [SplashScrollView setContentSize:CGSizeMake(3*addedView.frame.size.width, SplashScrollView.frame.size.height)];
                addedView.img.image = [UIImage imageNamed:@"ballboyws.png"];
                [AddNewChildButton setHidden:YES];
                [addedView.lbl setText:@"من أجل متابعة صحة اطفالك"];
            }
                break;
        }
        [SplashScrollView addSubview:addedView];
    }
}



#pragma -mark uiscrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = SplashScrollView.frame.size.width;
    int page = floor((SplashScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    SplashPageControl.currentPage = page;
    if (page == 0)
    {
        AddNewChildButton.hidden = NO;
    }
    else if (page == 1)
    {
        AddNewChildButton.hidden = YES;
    }
    else
    {
        AddNewChildButton.hidden = YES;
    }
    
}



- (IBAction)AddNewChildAction:(UIButton *)sender {
    @try {
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"first_use"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        AddChildViewController *vc_addChild = [[AddChildViewController alloc]init];
        vc_addChild.IsFirstUse=YES;
        [self.navigationController pushViewController:vc_addChild animated:YES];
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)SkipButtonAction:(UIButton *)sender {
    @try {
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"first_use"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        AddChildViewController *vc_addChild = [[AddChildViewController alloc]init];
        vc_addChild.IsFirstUse=YES;
        [self.navigationController pushViewController:vc_addChild animated:YES];
        
    }
    @catch (NSException *exception) {
        
    }
}
@end
