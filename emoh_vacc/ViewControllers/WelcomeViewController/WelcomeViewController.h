//
//  WelcomeViewController.h
//  emoh_vacc
//
//  Created by Assem Imam on 12/30/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeViewController : UIViewController
{
    __weak IBOutlet UIPageControl *SplashPageControl;
    __weak IBOutlet UIScrollView *SplashScrollView;
    __weak IBOutlet UIButton *AddNewChildButton;

}
- (IBAction)AddNewChildAction:(UIButton *)sender;

- (IBAction)SkipButtonAction:(UIButton *)sender;
@end
