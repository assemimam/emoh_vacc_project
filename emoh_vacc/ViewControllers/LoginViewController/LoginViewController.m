//
//  LoginViewController.m
//  emoh_vacc
//
//  Created by Assem Imam on 12/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "LoginViewController.h"
#import "Sharing.h"
#import "AddChildViewController.h"
#import "Webservice.h"
#import "VacinationListViewController.h"
#import "WelcomeViewController.h"
#import "AddChildViewController.h"

#define APP_CLIENT_ID_MS_LIVE @"0000000044136026"
#define TWITTER_CUSTOMER_KEY @"MxilGVBkNqHUDJwXwcqqBaWf3"
#define TWITTER_CUSTOMER_SECRET @"SLVZgUM6Ob4nxu8k4i8TiSTwGTCLh4vPmmHfJnz31sXe1cdGwr"
#define APP_CLIENT_ID_GOOGLE_PLUS @"400358023130-lg3dg1uvct3ds5ab2r833772oiphi3kh.apps.googleusercontent.com"

@interface LoginViewController ()<sharingDelegate>
{
    NSMutableDictionary*logedUserData;
    NSNotification *loginNotification ;
}
@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    loginNotification = [[NSNotification alloc]initWithName:LOGIN_NOTIFICATION object:nil userInfo:nil];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    logedUserData = [[NSMutableDictionary alloc]init];
    [super viewDidLoad];
    [[Sharing getObject] setDelegate:self];
    [[Sharing getObject] setCurrentSharingViewController:self];
    LoadingIndicator.numberOfCircles = 4;
    LoadingIndicator.radius = 10;
    LoadingIndicator.internalSpacing = 5;
    LoadingIndicator.duration = 0.5;
    LoadingIndicator.delay = 0.3;
    LoadingIndicator.delegate=self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)TwitterLoginAction:(UIButton *)sender {
    @try {
        if (internetConnection) {
            [self.view setUserInteractionEnabled:NO];
            [LoadingIndicator startAnimating];
            [[Sharing getObject]twitterLoginWithCustomerKey:TWITTER_CUSTOMER_KEY CustomerSecret:TWITTER_CUSTOMER_SECRET];
        }
        else{
            [[[UIAlertView alloc] initWithTitle:@""
                                        message:[[Language getObject]getStringWithKey:@"NoInternet"]
                                       delegate:nil
                              cancelButtonTitle:[[Language getObject]getStringWithKey:@"CancelTitle"]
                              otherButtonTitles: nil]show];

        }
       
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)MicrosoftLoginAction:(UIButton *)sender {
    @try {
        if (internetConnection) {
            [self.view setUserInteractionEnabled:NO];
            [LoadingIndicator startAnimating];
            [[Sharing getObject]liveLoginWithApplicationClientID:APP_CLIENT_ID_MS_LIVE];
        }
        else{
            [[[UIAlertView alloc] initWithTitle:@""
                                        message:[[Language getObject]getStringWithKey:@"NoInternet"]
                                       delegate:nil
                              cancelButtonTitle:[[Language getObject]getStringWithKey:@"CancelTitle"]
                              otherButtonTitles: nil]show];

        }
        
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)GooglePlusLoginAction:(UIButton *)sender {
    @try {
        if (internetConnection) {
            currentUserSocialMedia = googlePlusMediaType;
            [self.view setUserInteractionEnabled:NO];
            [LoadingIndicator startAnimating];
            [[Sharing getObject] googleLoginWithClientID:APP_CLIENT_ID_GOOGLE_PLUS];
        }
        else{
            [[[UIAlertView alloc] initWithTitle:@""
                                        message:[[Language getObject]getStringWithKey:@"NoInternet"]
                                       delegate:nil
                              cancelButtonTitle:[[Language getObject]getStringWithKey:@"CancelTitle"]
                              otherButtonTitles: nil]show];

        }
        
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)FaceBookLoginAction:(UIButton *)sender {
    @try {
        if (internetConnection) {
            [self.view setUserInteractionEnabled:NO];
            [LoadingIndicator startAnimating];
            [[Sharing getObject] faceBookLogin];
        }
        else{
            [[[UIAlertView alloc] initWithTitle:@""
                                       message:[[Language getObject]getStringWithKey:@"NoInternet"]
                                      delegate:nil
                             cancelButtonTitle:[[Language getObject]getStringWithKey:@"CancelTitle"]
                             otherButtonTitles: nil]show];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
#pragma -mark webservice methods
-(void)handleLoginWithUserInformationResult:(id)result
{
    @try {
        [self.view setUserInteractionEnabled:YES];
        [LoadingIndicator stopAnimating];
        if (result) {
            if ([[result objectForKey:@"IsSuccess"]intValue]==1) {
                
                 [logedUserData setObject:[NSString stringWithFormat:@"%i",currentUserSocialMedia] forKey:@"login_type"];
                [logedUserData setObject:[result objectForKey:@"NewUserId"]  forKey:@"user_id"];
                [logedUserData setObject:[result objectForKey:@"AuthenticationToken"]   forKey:@"user_token"];
                [[NSUserDefaults standardUserDefaults]setObject:logedUserData forKey:@"user_info"];

                [[NSUserDefaults standardUserDefaults]synchronize];
                [[NSNotificationCenter defaultCenter] postNotification:loginNotification];
                
                AddChildViewController *vc_addChild = [[AddChildViewController alloc]init];
                if ([[NSUserDefaults standardUserDefaults]objectForKey:@"first_use"]==nil) {
                    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"child_count"]) {
                        if ([[result objectForKey:@"Exists"] intValue]==1) {
                            
                            VacinationListViewController *vc_vaccinations = [[VacinationListViewController alloc]init];
                            [self.navigationController pushViewController:vc_vaccinations animated:YES];
                        }
                        else {
                            WelcomeViewController *vc_welcome=[[WelcomeViewController alloc]init];
                            [self.navigationController pushViewController:vc_welcome animated:YES];
                        }
                    }
                    else{
                         if ([[result objectForKey:@"Exists"] intValue]==1) {
                             vc_addChild.IsFirstUse=YES;
                         }
                        
   
                        self.navigationController.viewControllers = [NSArray arrayWithObject:vc_addChild];
                    }
                    
                 }
                else{
                     if ([[NSUserDefaults standardUserDefaults]objectForKey:@"child_count"]) {
                         VacinationListViewController *vc_vaccinations = [[VacinationListViewController alloc]init];
                         [self.navigationController pushViewController:vc_vaccinations animated:YES];

                     }
                     else{
                         self.navigationController.viewControllers = [NSArray arrayWithObject:vc_addChild];
                     }
                }
                
            }
            else{
                
            }
        }
    }
    @catch (NSException *exception) {
        
    }
}
-(void)loginWithUserInformation:(id)user_info
{
    @try {
        id result =[Webservice loginWithUserName:[user_info objectForKey:@"Name"] AccountToken:[user_info objectForKey:@"AccountToken"] PushNotification:[user_info objectForKey:@"PushNotification"] AlertPeriodByDays:[user_info objectForKey:@"AlertPeriodByDays"] MobileNumber:[user_info objectForKey:@"MobileNumber"] Name:[user_info objectForKey:@"Name"]];
        
        [self performSelectorOnMainThread:@selector(handleLoginWithUserInformationResult:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}
#pragma  -mark sharing delegate methods
-(void)liveAuthenticationFailedWithError:(NSError *)error
{
    @try {
        NSLog(@"user name or password error");
        [self.view setUserInteractionEnabled:YES];
        [LoadingIndicator stopAnimating];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)liveGetUserInformationFailedWithError:(NSError *)error{
    @try {
          NSLog(@"faild to get user data");
        [self.view setUserInteractionEnabled:YES];
        [LoadingIndicator stopAnimating];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)liveGetUserInformationSucceeded:(id)user_info
{
    @try {
        currentUserSocialMedia = microsoftLiveMediaType;
        
        NSMutableDictionary *parms = [[NSMutableDictionary alloc]init];
        [parms setObject:[user_info objectForKey:@"name"] forKey:@"Name"];
        [parms setObject:[user_info objectForKey:@"id"] forKey:@"AccountToken"];
        [parms setObject:@"false" forKey:@"PushNotification"];
        [parms setObject:@"0" forKey:@"AlertPeriodByDays"];
        [parms setObject:@"" forKey:@"MobileNumber"];
        [parms setObject:[user_info objectForKey:@"name"] forKey:@"UserName"];
        
        [logedUserData setObject:[user_info objectForKey:@"name"] forKey:@"name"];
        [logedUserData setObject:[user_info objectForKey:@"image"]  forKey:@"image"];
        
        [NSThread detachNewThreadSelector:@selector(loginWithUserInformation:) toTarget:self withObject:parms];
       
    }
    @catch (NSException *exception) {
        
    }
}
-(void)twitterAuthenticationFailedWithError:(NSError *)error
{
    @try {
        [self.view setUserInteractionEnabled:YES];
        [LoadingIndicator stopAnimating];
        [[[UIAlertView alloc]initWithTitle:@"تسجيل الدخول " message:@"برجاء ادخال بيانات حساب تويتر من اعدادات الهاتف" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil]show ];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)twitterGetUserInformationFailedWithError:(NSError *)error
{
    @try {
          NSLog(@"faild to get user data");
        [self.view setUserInteractionEnabled:YES];
        [LoadingIndicator stopAnimating];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)twitterGetUserInformationSucceeded:(id)user_info
{
    @try {
        currentUserSocialMedia = twitterMediaType;
        
        NSMutableDictionary *parms = [[NSMutableDictionary alloc]init];
        [parms setObject:[user_info objectForKey:@"userName"] forKey:@"Name"];
        [parms setObject:[user_info objectForKey:@"userID"] forKey:@"AccountToken"];
        [parms setObject:@"false" forKey:@"PushNotification"];
        [parms setObject:@"0" forKey:@"AlertPeriodByDays"];
        [parms setObject:@"" forKey:@"MobileNumber"];
        [parms setObject:[user_info objectForKey:@"userName"] forKey:@"UserName"];
        [logedUserData setObject:[user_info objectForKey:@"userName"] forKey:@"name"];
        [logedUserData setObject:[user_info objectForKey:@"image"]  forKey:@"image"];
        
        [NSThread detachNewThreadSelector:@selector(loginWithUserInformation:) toTarget:self withObject:parms];
    }
    @catch (NSException *exception) {
        
    }
  
}
-(void)googleAuthenticationFailedWithError:(NSError *)error{
    @try {
        [self.view setUserInteractionEnabled:YES];
        [LoadingIndicator stopAnimating];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)googleGetUserInformationFailedWithError:(NSError *)error{
    @try {
        [self.view setUserInteractionEnabled:YES];
        [LoadingIndicator stopAnimating];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)googleGetUserInformationSucceeded:(id)user_info
{
    @try {
        currentUserSocialMedia = googlePlusMediaType;
        NSMutableDictionary *parms = [[NSMutableDictionary alloc]init];
        [parms setObject:[user_info objectForKey:@"name"] forKey:@"Name"];
        [parms setObject:[user_info objectForKey:@"id"] forKey:@"AccountToken"];
        [parms setObject:@"false" forKey:@"PushNotification"];
        [parms setObject:@"0" forKey:@"AlertPeriodByDays"];
        [parms setObject:@"" forKey:@"MobileNumber"];
        [parms setObject:[user_info objectForKey:@"name"] forKey:@"UserName"];
        
        [logedUserData setObject:[user_info objectForKey:@"name"] forKey:@"name"];
        [logedUserData setObject:[user_info objectForKey:@"image"]  forKey:@"image"];
        
        [NSThread detachNewThreadSelector:@selector(loginWithUserInformation:) toTarget:self withObject:parms];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)faceBookAuthenticationFailedWithError:(NSError *)error{
    @try {
        [self.view setUserInteractionEnabled:YES];
        [LoadingIndicator stopAnimating];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)faceBookGetUserInformationFailedWithError:(NSError *)error{
    @try {
        [self.view setUserInteractionEnabled:YES];
        [LoadingIndicator stopAnimating];
    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)faceBookGetUserInformationSucceeded:(id)user_info
{
    @try {
        currentUserSocialMedia = faceBookMediaType;
        
        NSMutableDictionary *parms = [[NSMutableDictionary alloc]init];
        [parms setObject:[user_info objectForKey:@"name"] forKey:@"Name"];
        [parms setObject:[user_info objectForKey:@"id"] forKey:@"AccountToken"];
        [parms setObject:@"false" forKey:@"PushNotification"];
        [parms setObject:@"0" forKey:@"AlertPeriodByDays"];
        [parms setObject:@"" forKey:@"MobileNumber"];
        [parms setObject:[user_info objectForKey:@"name"] forKey:@"UserName"];
        
        [logedUserData setObject:[user_info objectForKey:@"name"] forKey:@"name"];
        [logedUserData setObject:[user_info objectForKey:@"image"] forKey:@"image"];
        
        [NSThread detachNewThreadSelector:@selector(loginWithUserInformation:) toTarget:self withObject:parms];
    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -amrk loading delegate
- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView
      circleBackgroundColorAtIndex:(NSUInteger)index {
    CGFloat red   = 57.0f/255.0;
    CGFloat green = 90.0f/255.0;
    CGFloat blue  = 147.0f/255.0;
    switch (index) {
        case 0:
        {
             red   = 57.0f/255.0;
             green = 90.0f/255.0;
             blue  = 147.0f/255.0;
        }
            break;
            
        case 1:
        {
            red   = 206.0f/255.0;
            green = 68.0f/255.0;
            blue  = 61.0f/255.0;
        }
            break;
        case 2:
        {
            red   = 88.0f/255.0;
            green = 169.0f/255.0;
            blue  = 216.0f/255.0;
        }
            break;
        case 3:
        {
            red   = 255.0f/255.0;
            green = 255.0f/255.0;
            blue  = 255.0f/255.0;
        }
            break;
            
    }
    // For a random background color for a particular circle
    
    CGFloat alpha = 1.0f;
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}
@end
