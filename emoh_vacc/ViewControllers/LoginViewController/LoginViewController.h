//
//  LoginViewController.h
//  emoh_vacc
//
//  Created by Assem Imam on 12/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MONActivityIndicatorView.h"

@interface LoginViewController : UIViewController<MONActivityIndicatorViewDelegate>
{
    
    __weak IBOutlet MONActivityIndicatorView *LoadingIndicator;
}
- (IBAction)TwitterLoginAction:(UIButton *)sender;
- (IBAction)MicrosoftLoginAction:(UIButton *)sender;
- (IBAction)GooglePlusLoginAction:(UIButton *)sender;
- (IBAction)FaceBookLoginAction:(UIButton *)sender;
@end
