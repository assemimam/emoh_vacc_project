//
//  AddChildViewController.m
//  emoh_vacc
//
//  Created by Assem Imam on 12/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "AddChildViewController.h"
#import "KeyValueField.h"
#import "LibrariesConstants.h"
#import "ChildrenListViewController.h"

#define ENABLE_SUMMIT @"didEnableAddChildButton"
#define GENDER @"النوع"
#define BIRTHDATE @"تاريخ الميلاد"

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0f)
#define IS_IPHONE_4 (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 480.0f)

@interface AddChildViewController ()<UINavigationControllerDelegate , UIImagePickerControllerDelegate, UITextFieldDelegate>
{
    int isChosen;
    BOOL isScrolled;
    NSString * isFirstSelected;
    UIImage *chosenChildImage;
    NSString *childBirthDate;
    NSString *childGender;
    UIImagePickerController *imagePicker;
    NSString *encodedChildPhoto;
    id genderList;
    NSString *userId;
    NSString *userName;
    UIActionSheet *genderActionSheet ;
    NSNotification *enableSubmitNotification;
}


@property (nonatomic ,strong) NSMutableArray *validationMessages;

-(IBAction)addChildImageButtonTouched:(id)sender;
-(IBAction)addChildGenderButtonTouched:(UIButton *)sender;
-(IBAction)addChildBirthDateButtonTouched:(UIButton *)sender;
-(IBAction)addNewChildButtonTouched:(id)sender;
-(IBAction)dismissKeyboard:(id)sender;
-(IBAction)dissmisViewController:(id)sender;
@end

@implementation AddChildViewController
@synthesize IsFirstUse;
-(void)dissmisViewController:(id)sender
{
    @try {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    @catch (NSException *exception) {
    
    }
    
}
-(void)didEnableAddChildButton
{
    @try {
        int childNameTextLength =[childNameTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""].length;
        
        if ((childNameTextLength > 0) && (![childGenderButton.titleLabel.text isEqualToString:GENDER])&& (![childBirthDateButton.titleLabel.text isEqualToString:BIRTHDATE]))
        {
           
            [AddNewChildButton setEnabled:YES];
            [AddNewChildButton setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
         
            
        }
        else{
            [AddNewChildButton setEnabled:NO];
        }
    }
    @catch (NSException *exception) {
        
    }
}
- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        userId =[[[NSUserDefaults standardUserDefaults]objectForKey:@"user_info"]objectForKey:@"user_id"];
        userName =[[[NSUserDefaults standardUserDefaults]objectForKey:@"user_info"]objectForKey:@"name"];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        genderList = [[Language getObject]getArrayWithKey:@"GenderTypes"];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didEnableAddChildButton)
                                                     name:ENABLE_SUMMIT
                                                   object:nil];
        
        
        if (IS_IPHONE_4)
        {
            [childNameTextField addTarget:self
                                   action:@selector(textFieldFinished:)
                         forControlEvents:UIControlEventEditingDidBegin];
        }
        
        [self roundProfilePhoto];

    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



#pragma mark - helping methods

-(void)textFieldFinished:(UITextField *)text
{
    scrollView.contentSize = CGSizeMake(320,172);
    CGPoint p=CGPointMake(0,50);
    [scrollView setContentOffset:p animated:NO];
}
-(void)rotateImage:(UIImageView *)image
{
    @try {
        image.transform = CGAffineTransformMakeRotation(M_PI);
    
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)roundProfilePhoto
{
    @try {
        [CommonMethods roundImgView:childImageView withCornerRadius:52.0];
        
    }
    @catch (NSException *exception) {
        
    }
    
    
}

- (NSString *)encodeImageToBase64String:(UIImage *)image
{
    @try
    {
        NSData *imgData =  UIImageJPEGRepresentation(image, 0.1);
        NSString *str  = [imgData base64Encoding];
        return str;
    }
    @catch (NSException *exception)
    {
    }
}


#pragma mark - button actions

-(IBAction)dismissKeyboard:(id)sender
{
    @try {
        [genderActionSheet dismissWithClickedButtonIndex:[genderList count] animated:YES];
        [UIView animateWithDuration:0.5 animations:^{
            [BirthDateView setFrame:CGRectMake(0, self.view.frame.size.height +BirthDateView.frame.size.height , BirthDateView.frame.size.width, BirthDateView.frame.size.height)];
        }];
        
        if (IS_IPHONE_4)
        {
            scrollView.contentSize = CGSizeMake(320,172);
            CGPoint p=CGPointMake(0,-1);
            [scrollView setContentOffset:p animated:YES];
            
        }
        [childNameTextField resignFirstResponder];
    }
    @catch (NSException *exception) {
        
    }

   
}

#pragma mark add child image

-(IBAction)addChildImageButtonTouched:(id)sender
{
    @try
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
    @catch (NSException *exception){
        
    }
}

#pragma mark add child Gender


-(IBAction)addChildGenderButtonTouched:(UIButton *)sender
{
    
    @try {
        [self dismissKeyboard:nil];
         genderActionSheet = [[UIActionSheet alloc]init];
        genderActionSheet.delegate = self;
        genderActionSheet.title=[[Language getObject]getStringWithKey:@"ChooseGenderTitle"];
        for (id gender in genderList) {
            [genderActionSheet addButtonWithTitle:[gender objectForKey:@"name"]];
        }
        [genderActionSheet addButtonWithTitle:[[Language getObject]getStringWithKey:@"CancelTitle"]];
        [genderActionSheet setCancelButtonIndex:[genderList count]];
        [genderActionSheet showInView:self.view];
    }
    @catch (NSException *exception) {
        
    }
}

#pragma mark add child birthdate

-(IBAction)addChildBirthDateButtonTouched:(UIButton *)sender
{
    @try {
        [self dismissKeyboard:nil];
        
        [UIView animateWithDuration:0.5 animations:^{
            [BirthDateView setFrame:CGRectMake(0, self.view.frame.size.height -BirthDateView.frame.size.height , BirthDateView.frame.size.width, BirthDateView.frame.size.height)];
        }];
    }
    @catch (NSException *exception) {
        
    }
}
#pragma mark add New child

-(IBAction)addNewChildButtonTouched:(id)sender
{
    @try {
  
        [self dismissKeyboard:nil];
        if (internetConnection) {
           
            if ([self ValidateData]){
                SHOW_LOADING;
                if (chosenChildImage != nil)
                {
                    NSString *photoString = [self encodeImageToBase64String:chosenChildImage];
                    NSString *imgEncodedString = [CommonMethods urlencode:photoString];
                    [NSThread detachNewThreadSelector:@selector(getEncodedImageString:) toTarget:self withObject:imgEncodedString];
                }
                else{
                 
                    
                    [NSThread detachNewThreadSelector:@selector(addNewChild) toTarget:self withObject:nil];
                }
                
            }
            else
            {
                [self showValidationMessages];
            }
        }
        else
        {
            [[[UIAlertView alloc] initWithTitle:@""
                                        message:[[Language getObject]getStringWithKey:@"NoInternet"]
                                       delegate:nil
                              cancelButtonTitle:[[Language getObject]getStringWithKey:@"CancelTitle"]
                              otherButtonTitles: nil]show];

        }

        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)handleGetEncodedImageStringResult:(id)result
{
    @try {
        if (result) {
            if ([result objectForKey:@"Uri"]) {
                encodedChildPhoto = [result objectForKey:@"Uri"];
                [NSThread detachNewThreadSelector:@selector(addNewChild) toTarget:self withObject:nil];
            }
        }
        
    }
    @catch (NSException *exception) {
        
    }
}
-(void)getEncodedImageString:(NSString*)imgEncodedString
{
    @try {
        
        id result=  [Webservice getEncodedImgWithChildName:childNameTextField.text UserName:userName ImageString:imgEncodedString UserId:[userId intValue]];
     
        [self performSelectorOnMainThread:@selector(handleGetEncodedImageStringResult:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}

-(void)addNewChild
{
    @try {
        
        id  result  =  [Webservice addNewChildWithUserId:[userId intValue]
                                               ChildName:childNameTextField.text
                                                  Gender:childGender.intValue
                                               birthDate:[CommonMethods convertDateToString:BirthDatePicker.date withFormat:@"dd-MM-yyyy"]
                                                   Photo:encodedChildPhoto];
        
        [self performSelectorOnMainThread:@selector(handleAddNewChild:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}


-(void)handleAddNewChild:(id)result
{
    @try {
        HIDE_LOADING;
        if (result)
        {
            if ([[result objectForKey:@"IsSuccess"]intValue]==1)
            {
                NSLog(@"success");
                int childCount;
                if ([[NSUserDefaults standardUserDefaults]objectForKey:@"child_count"]) {
                    childCount =[[[NSUserDefaults standardUserDefaults]objectForKey:@"child_count"]intValue];
                    if (childCount<10) {
                         childCount++;
                    }
                }
                else{
                    childCount = 1;
                }
                
                [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"%i",childCount] forKey:@"child_count"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                if (IsFirstUse) {
                    ChildrenListViewController *vc_children = [[ChildrenListViewController alloc]init];
                    
                    [self.navigationController pushViewController:vc_children animated:YES];
                    
                }
                else{
                  [self dismissViewControllerAnimated:YES completion:nil];
                }
               
            }
            else
            {
                [[[UIAlertView alloc]initWithTitle:nil message:@"برجاء المحاولة  مرة اخرى" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil]show];
                
            }
            
        }
    }
    @catch (NSException *exception) {
        
    }
    
}

#pragma mark - UIImagePicker Delegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    @try
    {
        chosenChildImage = info[UIImagePickerControllerEditedImage];
        [childImageView setImage:chosenChildImage];
      
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    @catch (NSException *exception)
    {
        
    }
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    @try
    {
        [picker dismissViewControllerAnimated:YES completion:NULL];
    }
    @catch (NSException *exception)
    {
        
    }
}



#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    @try {

        if (IS_IPHONE_4)
        {
            scrollView.contentSize = CGSizeMake(320,172);
            CGPoint p=CGPointMake(0,-1);
            [scrollView setContentOffset:p animated:YES];
            
        }
        
        [textField resignFirstResponder];
        [self performSelector:@selector(postNotificationSubmit) withObject:nil afterDelay:0.5];
        return NO;
    }
    @catch (NSException *exception) {
        
    }
    
    
}

#pragma mark - validate data


-(NSMutableArray *)validationMessages
{
    @try {
        
        if (!_validationMessages)
        {
            _validationMessages = [NSMutableArray array];
            
        }
        
        return _validationMessages;
    }
    @catch (NSException *exception)
    {
    }
}

-(void)showValidationMessages
{
    @try {
        NSString *message = [NSString new];
        for (int i = 0; i < self.validationMessages.count; i++)
        {
            if (self.validationMessages.count ==3)
            {
                message = @"من فضلك ادخل جميع البيانات المطلوبة";
            }
            else{
                message = [message stringByAppendingFormat:@"%@\n", self.validationMessages[i]];
            }
            
        }
        
        [[[UIAlertView alloc] initWithTitle:@""
                                    message:message
                                   delegate:nil
                          cancelButtonTitle:@"تم"
                          otherButtonTitles:nil] show];
    }
    @catch (NSException *exception) {
    }
}


-(BOOL)ValidateData
{
    @try
    {
        [self.validationMessages removeAllObjects];
        
        if (![CommonMethods isValidString:childNameTextField.text])
        {
            [self.validationMessages addObject:@"من فضلك ادخل اسم الطفل"];
        }
        if ([childGenderButton.titleLabel.text isEqualToString:GENDER])
        {
            [self.validationMessages addObject:@"من فضلك ادخل النوع"];
        }
        if ([childBirthDateButton.titleLabel.text isEqualToString:BIRTHDATE])
        {
            [self.validationMessages addObject:@"من فضلك ادخل تاريخ الميلاد"];
        }
   
        
        return (self.validationMessages.count == 0);
        
    }
    @catch (NSException *exception)
    {
        
    }
}


#pragma mark - pickerView hide/show

- (void)pickerWillShow:(NSNotification *)notification
{
    @try {
        
        if (IS_IPHONE_4)
        {
            CGRect R;
            [[[notification userInfo]objectForKey:childGenderButton]getValue:&R];
            scrollView.contentSize = CGSizeMake(320,172);
            
            CGPoint p=CGPointMake(0,78);
            [scrollView setContentOffset:p animated:YES];
            
            
        }
        else
        {
            CGRect R;
            [[[notification userInfo]objectForKey:childGenderButton]getValue:&R];
            scrollView.contentSize = CGSizeMake(320,172);
            CGPoint p=CGPointMake(0,40);
            [scrollView setContentOffset:p animated:YES];
        }
    }
    @catch (NSException *exception) {
        
    }
}

-(void)pickerWillHide:(NSNotification *)notification
{
    @try {
        if (IS_IPHONE_4)
        {
            scrollView.contentSize = CGSizeMake(320,172);
            CGPoint p=CGPointMake(0,0);
            [scrollView setContentOffset:p animated:YES];
        }
        else
        {
            scrollView.contentSize = CGSizeMake(320,172);
            CGPoint p=CGPointMake(0,-20);
            [scrollView setContentOffset:p animated:YES];
        }
        
    }
    @catch (NSException *exception) {
        
    }
}

- (IBAction)CancelBirthDate:(UIBarButtonItem *)sender {
    @try {
        [UIView animateWithDuration:0.5 animations:^{
            [BirthDateView setFrame:CGRectMake(0, self.view.frame.size.height + BirthDateView.frame.size.height , BirthDateView.frame.size.width, BirthDateView.frame.size.height)];
        }];

    }
    @catch (NSException *exception) {
        
    }
    
}
-(void)postNotificationSubmit
{
    enableSubmitNotification = [[NSNotification alloc]initWithName:ENABLE_SUMMIT object:nil userInfo:nil];
    
    [[NSNotificationCenter defaultCenter] postNotification:enableSubmitNotification];
}
- (IBAction)OkBirthDateAction:(UIBarButtonItem *)sender {
    @try {
        
        if ([BirthDatePicker.date compare:[NSDate date]]==NSOrderedDescending) {
            [[[UIAlertView alloc]initWithTitle:@"خطأ" message:@"  تاريخ الميلاد يجب ان يكون اقل من تاريخ اليوم" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil]show];
            
            return;
        }
        
        NSTimeInterval intervalSeconeds = [[NSDate date] timeIntervalSinceDate:BirthDatePicker.date];
        int intervalYears = (((60*60)*24)*30)*12;
        float childAge =(intervalSeconeds/intervalYears);
        
        if (childAge>6.08611155) {
            [[[UIAlertView alloc]initWithTitle:@"خطأ" message:@"لابد ان لا يتعدى عمر الطفل عن 6 سنوات" delegate:nil cancelButtonTitle:@"موافق" otherButtonTitles:nil, nil]show];
            return;
        }
        
        [UIView animateWithDuration:0.5 animations:^{
            [BirthDateView setFrame:CGRectMake(0, self.view.frame.size.height +BirthDateView.frame.size.height , BirthDateView.frame.size.width, BirthDateView.frame.size.height)];
        }];
        
        [childBirthDateButton setTitle:[CommonMethods convertDateToString:BirthDatePicker.date withFormat:@"dd MMMM yyyy"]  forState:UIControlStateNormal];
        
        [self performSelector:@selector(postNotificationSubmit) withObject:nil afterDelay:0.5];

    }
    @catch (NSException *exception) {
        
    }
    
}
#pragma -mark uiPicker delegate

#pragma -mark uiactionsheet delegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    @try {
       
        [self performSelector:@selector(postNotificationSubmit) withObject:nil afterDelay:0.5];
        
        if (buttonIndex<[genderList count]) {
            [childGenderButton setTitle:[[genderList objectAtIndex:buttonIndex]objectForKey:@"name"] forState:UIControlStateNormal];
            childGenderButton.tag =[[[genderList objectAtIndex:buttonIndex]objectForKey:@"value"]intValue];
            childGender = [NSString stringWithFormat:@"%i",buttonIndex];
        }
    }
    @catch (NSException *exception) {
        
    
    }
}
@end
