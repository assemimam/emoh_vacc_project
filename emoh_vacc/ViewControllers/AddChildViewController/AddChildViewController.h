//
//  AddChildViewController.h
//  emoh_vacc
//
//  Created by Assem Imam on 12/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//


@interface AddChildViewController : UIViewController<UIActionSheetDelegate,UITextFieldDelegate>
{
    __weak IBOutlet UIButton *AddNewChildButton;
    IBOutlet UIView *BirthDateView;
    __weak IBOutlet UIDatePicker *BirthDatePicker;
    __weak IBOutlet UIScrollView *scrollView;
    __weak IBOutlet UIImageView *childImageView;
    __weak IBOutlet UIImageView *arrowImage;
    __weak IBOutlet UITextField *childNameTextField;
    __weak IBOutlet UIButton *childGenderButton;
    __weak IBOutlet UIButton *childBirthDateButton;
}
- (IBAction)CancelBirthDate:(UIBarButtonItem *)sender;
- (IBAction)OkBirthDateAction:(UIBarButtonItem *)sender;
@property(nonatomic)BOOL IsFirstUse;
@property (nonatomic ,strong) NSString *userId;
@end
