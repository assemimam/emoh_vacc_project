//
//  MenuViewController.h
//  emoh_vacc
//
//  Created by Assem Imam on 12/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NPRImageView.h"

@interface MenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    
    __weak IBOutlet UIView *HeaderView;
    __weak IBOutlet UILabel *UserNameLabel;
    __weak IBOutlet NPRImageView *UserImageView;
    __weak IBOutlet UIImageView *SocialMediaImageView;
}
@end
