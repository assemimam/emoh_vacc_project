//
//  MenuViewController.m
//  emoh_vacc
//
//  Created by Assem Imam on 12/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "MenuViewController.h"
#import "Language.h"
#import "MenuCell.h"
#import "VacinationListViewController.h"
#import "ChildrenListViewController.h"
#import "SettingsViewController.h"

#define VACCINATION_INDIX 0
#define CHILDREN_INDIX 1
#define SETTINGS_INDIX 2

@interface MenuViewController ()
{
    NSMutableArray *menuListItems;
}
@end

@implementation MenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)didFinishUserLogin
{
    @try {
        [self manipulateUserInformation];
    }
    @catch (NSException *exception) {
        
    }
    
}
- (void)manipulateUserInformation
{
    @try {
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"user_info"]) {
            NSString*userName =[[[NSUserDefaults standardUserDefaults]objectForKey:@"user_info"]objectForKey:@"name"];
            UserNameLabel.text =(userName!=nil)?userName:@"";
            int userLoginType =[[[[NSUserDefaults standardUserDefaults]objectForKey:@"user_info"]objectForKey:@"login_type"]intValue];
            UserNameLabel.text =(userName!=nil)?userName:@"";
            switch (userLoginType) {
                case faceBookMediaType:
                {
                    SocialMediaImageView.image=[UIImage imageNamed:@"fbdefaultic.png"];
                }
                    break;
                    
                case twitterMediaType:
                {
                    SocialMediaImageView.image=[UIImage imageNamed:@"twiiterdefaultic.png"];
                }
                    break;
                case microsoftLiveMediaType:
                {
                    SocialMediaImageView.image=[UIImage imageNamed:@"microsoftdefaultic.png"];
                }
                    break;
                case googlePlusMediaType:
                {
                    SocialMediaImageView.image=[UIImage imageNamed:@"googledefaultic.png"];
                }
                    break;
            }
            NSString *imageUrlString =[[[NSUserDefaults standardUserDefaults]objectForKey:@"user_info"]objectForKey:@"image"];
            NSURL *imageUrl = [NSURL URLWithString:[imageUrlString  stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            if (imageUrl) {
                [UserImageView setImageWithContentsOfURL:imageUrl placeholderImage:[UIImage imageNamed:@"deaultimage.png"]];
            }
        }

    }
    @catch (NSException *exception) {
        
    }
}

- (void)viewDidLoad
{
    @try {
        [super viewDidLoad];
        menuListItems = [[NSMutableArray alloc]init];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(didFinishUserLogin)
                                                     name:LOGIN_NOTIFICATION
                                                   object:nil];
        
        id menuList =[[Language getObject]getArrayWithKey:@"MenuItems"];
        for (id item in menuList) {
            NSMutableDictionary *itemDic = [[NSMutableDictionary alloc]init];
            [itemDic setObject:[item objectForKey:@"image"] forKey:@"image"];
            [itemDic setObject:[item objectForKey:@"selected"] forKey:@"selected"];
            [itemDic setObject:[item objectForKey:@"title"] forKey:@"title"];
            
            [menuListItems addObject:itemDic];
        }
        CGSize screenSize = [[UIScreen mainScreen] bounds].size;
        [self manipulateUserInformation];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, screenSize.height)];
        }
        
        //make user image rounded
        UserImageView.layer.cornerRadius = UserImageView.frame.size.width / 2;
        UserImageView.clipsToBounds = YES;
        [HeaderView sendSubviewToBack:UserImageView];

    }
    @catch (NSException *exception) {
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCell *cell;
    @try {
        cell= (MenuCell*)[tableView dequeueReusableCellWithIdentifier:@"cleMenu"];
        if (cell==nil) {
            cell= (MenuCell*)[[[NSBundle mainBundle]loadNibNamed:@"MenuCell" owner:nil options:nil]objectAtIndex:0];
        }
        cell.MenuItemLabel.text = [[menuListItems objectAtIndex:indexPath.row] objectForKey:@"title"];
        cell.MenuItemImageView.image = [UIImage imageNamed:[[menuListItems objectAtIndex:indexPath.row] objectForKey:@"image"]];
        if ([[[menuListItems objectAtIndex:indexPath.row] objectForKey:@"selected"]intValue]==1) {
            cell.MenuItemLabel.textColor = [UIColor colorWithRed:241.0f/255.0f green:90.0f/255.0f blue:44.0f/255.0f alpha:1];
        }
        else{
            cell.MenuItemLabel.textColor = [UIColor colorWithRed:255.0f/255.0f green:255.0f/255.0f blue:255.0f/255.0f alpha:1];
        }
        UIView *selectionView = [[UIView alloc]initWithFrame:cell.frame];
        selectionView.backgroundColor = [UIColor clearColor];
        cell.selectedBackgroundView = selectionView;
    }
    @catch (NSException *exception) {
        
    }

    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [menuListItems count];
}
#pragma -mark uitableview delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        for (id item in menuListItems) {
            [item  setObject:@"0" forKey:@"selected"];
        }
        [[menuListItems objectAtIndex:indexPath.row] setObject:@"1" forKey:@"selected"];
        [tableView reloadData];
        UIViewController*vc_pushed;
        switch (indexPath.row) {
            case VACCINATION_INDIX:
            {
                vc_pushed = [[VacinationListViewController alloc]init];
            }
                break;
            case CHILDREN_INDIX:
            {
                 vc_pushed = [[ChildrenListViewController alloc]init];
            }
                break;
            case SETTINGS_INDIX:
            {
                 vc_pushed = [[SettingsViewController alloc]init];
            }
                break;
                
        }

        UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
        NSArray *controllers = [NSArray arrayWithObject:vc_pushed];
        navigationController.viewControllers = controllers;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
    @catch (NSException *exception) {
        
    }
   
}
@end
