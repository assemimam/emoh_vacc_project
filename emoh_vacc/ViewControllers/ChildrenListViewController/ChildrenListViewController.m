//
//  ChildrenListViewController.m
//  emoh_vacc
//
//  Created by Assem Imam on 12/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "ChildrenListViewController.h"
#import "AddChildViewController.h"
#import "ChildProfileViewController.h"
#import "ChildCell.h"
#import "ImageCache.h"

@interface ChildrenListViewController ()
{
    id childrenList;
}
@end

@implementation ChildrenListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     SHOW_LOADING
    [NSThread detachNewThreadSelector:@selector(getChlidren) toTarget:self withObject:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)handleGetChlidrenRelut:(id)result
{
    @try {
        HIDE_LOADING;
        if (result) {
            if ([[result objectForKey:@"IsSuccess"]intValue]==1) {
                if ([result objectForKey:@"Children"]) {
                    childrenList =[result objectForKey:@"Children"];
                    [ChildrenTableView reloadData];
                }
            }
            
          
        }
    }
    @catch (NSException *exception) {
        
    }
}
-(void)getChlidren
{
    @try {
        
        NSString *user_id = [[[NSUserDefaults standardUserDefaults]objectForKey:@"user_info"]objectForKey:@"user_id"];
        id result = [Webservice getChildrenListByUserID:user_id];
        [self performSelectorOnMainThread:@selector(handleGetChlidrenRelut:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
}
-(void)handledDleteChildWithIDResult:(id)result
{
    @try {
    
        if (result) {
            if ([[result objectForKey:@"IsSuccess"]intValue]==1) {
                [NSThread detachNewThreadSelector:@selector(getChlidren) toTarget:self withObject:nil];
            }
        }
    }
    @catch (NSException *exception) {
        
    }
    
}

-(void)deleteChildWithID:(NSString*)child_id
{
    @try {
          NSString *user_id = [[[NSUserDefaults standardUserDefaults]objectForKey:@"user_info"]objectForKey:@"user_id"];
        id result = [Webservice deleteChildWithChildID:child_id UserID:user_id];
        [self performSelectorOnMainThread:@selector(handledDleteChildWithIDResult:) withObject:result waitUntilDone:NO];
    }
    @catch (NSException *exception) {
        
    }
   
}
- (IBAction)AddChildButtonAction:(UIButton *)sender {
    @try {
        
//        ChildProfileViewController *Vc_ChildProfile = [[ChildProfileViewController alloc] initWithNibName:@"ChildProfileViewController" bundle:nil];
//        
//        [self.navigationController pushViewController:Vc_ChildProfile animated:YES];
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"child_count"]) {
            if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"child_count"]intValue]>=10) {
                
            }
            else{
                AddChildViewController *vc_addChild = [[AddChildViewController alloc]init];
                [self presentViewController:vc_addChild animated:YES completion:nil];
            }
        }
        else{
            AddChildViewController *vc_addChild = [[AddChildViewController alloc]init];
            [self presentViewController:vc_addChild animated:YES completion:nil];
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
   
}
#pragma -mark uitableview datasource methods
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ChildCell *cell;
    @try {
       // cell = (ChildCell*)[tableView dequeueReusableCellWithIdentifier:@"cleChild"];
        if (cell==nil) {
            cell =[[[NSBundle mainBundle]loadNibNamed:@"ChildCell" owner:nil options:nil]objectAtIndex:0];
        }
        int childGender =[[childrenList objectAtIndex: indexPath.row]objectForKey:@"Gender"]!=nil?[[[childrenList objectAtIndex: indexPath.row]objectForKey:@"Gender"]intValue]:0;
        NSString *childName = [[childrenList objectAtIndex: indexPath.row]objectForKey:@"ChildName"]!=nil?[[childrenList objectAtIndex: indexPath.row]objectForKey:@"ChildName"]:@"";
        NSString *birthDate = [[childrenList objectAtIndex: indexPath.row]objectForKey:@"BirthDateFormatted"]!=nil?[[childrenList objectAtIndex: indexPath.row]objectForKey:@"BirthDateFormatted"]:@"";
        
        NSString *noOfVaccinations =[NSString stringWithFormat:@"%i", [[[childrenList objectAtIndex: indexPath.row]objectForKey:@"VaccinationsCount"]intValue]]!=nil?[NSString stringWithFormat:@"%i", [[[childrenList objectAtIndex: indexPath.row]objectForKey:@"VaccinationsCount"]intValue]]:@"";
        
        cell.VaccinationsContainerView.layer.cornerRadius = cell.VaccinationsContainerView.frame.size.width / 2;
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        [dateFormatter setDateFormat:@"dd-MM-yyyy"];
        
        cell.VaccinationsContainerView.clipsToBounds = YES;
        [cell.contentView sendSubviewToBack:cell.ChildImageView];
        [cell.contentView sendSubviewToBack:cell.ChildGenderImageView];
        [cell.contentView bringSubviewToFront:cell.VaccinationsContainerView];
        cell.ChildNameLabel.text =childName;
        cell.BirthDateLabel.text = [CommonMethods convertDateToString:[dateFormatter dateFromString:birthDate] withFormat:@"dd MMMM yyyy"];
        cell.NoOfVaccinationsLabel.text =noOfVaccinations;
        if (childGender==0) {
            cell.ChildGenderImageView.image = [UIImage imageNamed:@"boycircle.png"];
            cell.LoadingActivityIndicator.color = [UIColor colorWithRed:36/255.0f green:158/255.0f blue:203/255.0f alpha:1];
        }
        else if (childGender==1)
        {
            cell.ChildGenderImageView.image = [UIImage imageNamed:@"boygirl.png"];
            cell.LoadingActivityIndicator.color = [UIColor colorWithRed:227/255.0f green:69/255.0f blue:98/255.0f alpha:1];
        }
        
        NSString *blaceholderImageName =@"Defaultimage.png";
        
        
        NSURL *imageUrl = [NSNull null]!=[[childrenList objectAtIndex: indexPath.row]objectForKey:@"Photo"]?[NSURL URLWithString:[[childrenList objectAtIndex: indexPath.row]objectForKey:@"Photo"]]:nil;
        [cell.LoadingActivityIndicator startAnimating];
        if (imageUrl) {
            
            [[ImageCache sharedInstance]downloadImageAtURL:imageUrl completionHandler:^(UIImage *image) {
                if (image) {
                    cell.ChildImageView.image = image;
                }
                else{
                    cell.ChildImageView.image = [UIImage imageNamed:blaceholderImageName];
                }
                 [cell.LoadingActivityIndicator stopAnimating];
            }];

        }
        else{
            cell.ChildImageView.image = [UIImage imageNamed:blaceholderImageName];
            [cell.LoadingActivityIndicator stopAnimating];
        }
        cell.ChildImageView.layer.cornerRadius = cell.ChildImageView.frame.size.width / 2;
        cell.ChildImageView.clipsToBounds = YES;
        [cell.contentView sendSubviewToBack:cell.ChildImageView];
        [cell.contentView sendSubviewToBack:cell.ChildGenderImageView];
        [cell.contentView bringSubviewToFront:cell.VaccinationsContainerView];
        cell.leftUtilityButtons = [self configureSeipeDeleteButtonByGender:childGender];
        cell.delegate = self;
    }
    @catch (NSException *exception) {
        
    }
    return cell;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  [childrenList count];
}

#pragma -mark uitableview delegate methods
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try {
        
    }
    @catch (NSException *exception) {
        
    }
}

#pragma mark - SWTableViewDelegate
- (NSArray *)configureSeipeDeleteButtonByGender:(int)gender
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    UIColor*buttonColor =[UIColor clearColor];
    if (gender==0) {
        buttonColor =[UIColor colorWithRed:36/255.0f green:158/255.0f blue:203/255.0f alpha:1];
    }
    else if (gender==1){
          buttonColor =[UIColor colorWithRed:234/255.0f green:120/255.0f blue:142/255.0f alpha:0.8];
    }
    [rightUtilityButtons sw_addUtilityButtonWithColor:buttonColor  title:@"حذف"];
    
    return rightUtilityButtons;
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index
{
    @try {
        
        if ([childrenList count]==0) {
            //didplay
        }
        else{
            NSIndexPath *cellIndexPath = [ChildrenTableView indexPathForCell:cell];
            NSString*childID = [[childrenList objectAtIndex:cellIndexPath.row]objectForKey:@"Id"];
            SHOW_LOADING;
            [NSThread detachNewThreadSelector:@selector(deleteChildWithID:) toTarget:self withObject:childID];
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    @try {
        
        if ([childrenList count]==0) {
           //didplay
        }
        else{
            NSIndexPath *cellIndexPath = [ChildrenTableView indexPathForCell:cell];
            NSString*childID = [[childrenList objectAtIndex:cellIndexPath.row]objectForKey:@""];
            SHOW_LOADING;
            [NSThread detachNewThreadSelector:@selector(deleteChildWithID:) toTarget:self withObject:childID];
        }
    }
    @catch (NSException *exception) {
        
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return YES;
            break;
        case 2:
            // set to NO to disable all right utility buttons appearing
            return NO;
            break;
        default:
            break;
    }
    
    return YES;
}




@end
