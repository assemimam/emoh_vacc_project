//
//  ChildrenListViewController.h
//  emoh_vacc
//
//  Created by Assem Imam on 12/27/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "MasterViewController.h"
#import "SWTableViewCell.h"

@interface ChildrenListViewController : MasterViewController<UITableViewDelegate,UITableViewDataSource,SWTableViewCellDelegate>
{
    
    __weak IBOutlet UITableView *ChildrenTableView;
}
- (IBAction)AddChildButtonAction:(UIButton *)sender;

@end
