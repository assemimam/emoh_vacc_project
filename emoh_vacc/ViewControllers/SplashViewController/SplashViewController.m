//
//  SplashViewController.m
//  emoh_vacc
//
//  Created by Assem Imam on 12/30/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "SplashViewController.h"
#import "LoginViewController.h"
#import "VacinationListViewController.h"
#import "AddChildViewController.h"


@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden=YES;
    [self performSelector:@selector(gotoNextViewController) withObject:nil afterDelay:3];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)gotoNextViewController
{
    @try {
        VacinationListViewController *vc_vacinations = [[VacinationListViewController alloc]init];
        LoginViewController *vc_login = [[LoginViewController alloc]init];
        AddChildViewController *vc_addChild = [[AddChildViewController alloc]init];
        
        if ([[NSUserDefaults standardUserDefaults]objectForKey:@"user_info"]) {
            if ([[NSUserDefaults standardUserDefaults]objectForKey:@"child_count"]) {
                self.navigationController.viewControllers = [NSArray arrayWithObject:vc_vacinations];
            }
            else{
                self.navigationController.viewControllers = [NSArray arrayWithObject:vc_addChild];
            }
        }
        else{
             self.navigationController.viewControllers = [NSArray arrayWithObject:vc_login];
        }

    }
    @catch (NSException *exception) {
        
    }
   
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
