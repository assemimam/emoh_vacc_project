//
//  TestViewController.h
//  emoh_vacc
//
//  Created by Assem Imam on 12/25/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sharing.h"

@interface TestViewController : UIViewController<sharingDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *testImageView;

@end
