//
//  AppDelegate.m
//  emoh_vacc
//
//  Created by Assem Imam on 12/24/14.
//  Copyright (c) 2014 Nuitex. All rights reserved.
//

#import "AppDelegate.h"
#import "Sharing.h"
#import "MenuViewController.h"
#import "LoginViewController.h"
#import "VacinationListViewController.h"
#import "SplashViewController.h"
#import "LibrariesConstants.h"
#import "AddChildViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    for (NSString *familyName in [UIFont familyNames]) {
        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
            NSLog(@"%@", fontName);
        }
    }
    [[Language getObject] setLanguage:languageArabic];
#pragma -marknetwork change notifications
    ////////////////////////////////////////////////
    reachabilityObserver = @"reachabilityObserver";
    Reachability *internetReach = [Reachability reachabilityForInternetConnection];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(internetReachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    [internetReach startNotifier];
    NSLog(@"%hhd", internetReach.isReachable);
    if (internetReach.currentReachabilityStatus == NotReachable)
    {
        internetConnection = NO;
        reachiblityAlertView =[[UIAlertView alloc] initWithTitle:@""
                                                         message:[[Language getObject]getStringWithKey:@"NoInternetReachable"]
                                                        delegate:nil
                                               cancelButtonTitle:[[Language getObject]getStringWithKey:@"CancelTitle"]
                                               otherButtonTitles: nil];
        [reachiblityAlertView show];
    }
    else
    {
        internetConnection = YES;
    }

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
        SplashViewController *vc_splash = [[SplashViewController alloc]init];
        LoginViewController *vc_login = [[LoginViewController alloc]init];
        VacinationListViewController *vc_vacinations = [[VacinationListViewController alloc]init];
        MenuViewController *vc_menu = [[MenuViewController alloc]init];
        AddChildViewController *vc_addChild = [[AddChildViewController alloc]init];
    
        if (!IS_DEVICE_RUNNING_IOS_AND_ABOVE(@"8")) {
            self.AppNavigationController.viewControllers = [NSArray arrayWithObject:vc_splash];
        }
        else{
            if ([[NSUserDefaults standardUserDefaults]objectForKey:@"user_info"]) {
               if ([[NSUserDefaults standardUserDefaults]objectForKey:@"child_count"]) {
                    self.AppNavigationController.viewControllers = [NSArray arrayWithObject:vc_vacinations];
                }
               else{
                   vc_addChild.IsFirstUse=YES;
                    self.AppNavigationController.viewControllers = [NSArray arrayWithObject:vc_addChild];
               }
            }
            else{
                 self.AppNavigationController.viewControllers = [NSArray arrayWithObject:vc_login];
            }
        }

        MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController containerWithCenterViewController:self.AppNavigationController leftMenuViewController:nil rightMenuViewController:vc_menu];
        self.AppNavigationController.navigationBarHidden=YES;
       self.window.rootViewController = container;
      [self.window makeKeyAndVisible];
      
       return YES;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication  annotation:(id)annotation {
    BOOL retValue;
    if (currentUserSocialMedia==googlePlusMediaType) {
        retValue = [GPPURLHandler handleURL:url
                      sourceApplication:sourceApplication
                             annotation:annotation];
    }
    else if (currentUserSocialMedia==faceBookMediaType){
        retValue= [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    }
    return retValue;
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
//    [FBSession openActiveSessionWithReadPermissions:@[@"public_profile", @"email"]
//                                       allowLoginUI:NO
//                                  completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
//                                      
//                                      
//                                  }];
//    
//    if ([FBSession activeSession].state == FBSessionStateCreatedTokenLoaded)
//    {
//        [[Sharing getObject] faceBookLogin];
//    }
//    
//    [FBAppCall handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
#pragma -mark appdelegate methods
-(void)showLoadingView:(BOOL)show
{
    [CommonMethods FormatView:LoadingView];
    if (show) {
        [LoadingView setHidden:NO];
        [self.window bringSubviewToFront:LoadingView];
        [LoadingActivityIndicator startAnimating];
    }
    else{
        [LoadingActivityIndicator stopAnimating];
        [LoadingView setHidden:YES];
        [self.window sendSubviewToBack:LoadingView];
    }
}
#pragma mark - Reachability

- (void)internetReachabilityChanged:(NSNotification *)note
{
    //commit
    NetworkStatus ns = [(Reachability *)[note object] currentReachabilityStatus];
    
    if (ns == NotReachable)
    {
        
        if (internetConnection)
        {
            internetConnection = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:reachabilityObserver object:nil];
            
            if (!reachiblityAlertView)
            {
                reachiblityAlertView =[[UIAlertView alloc] initWithTitle:@""
                                                                 message:[[Language getObject]getStringWithKey:@"NoInternetReachable"]
                                                                delegate:nil
                                                       cancelButtonTitle:[[Language getObject]getStringWithKey:@"CancelTitle"]
                                                       otherButtonTitles: nil];
            }
            [reachiblityAlertView show];
        }
    }
    else
    {
        //check for both with/out internet connection
        [reachiblityAlertView dismissWithClickedButtonIndex:0 animated:YES];
        
        if (!internetConnection)
        {
            internetConnection = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:reachabilityObserver object:nil];
        }
    }
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    reachiblityAlertView = nil;
}
#pragma -amrk loading delegate
- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView
      circleBackgroundColorAtIndex:(NSUInteger)index {
    // For a random background color for a particular circle
    CGFloat red   = (arc4random() % 256)/255.0;
    CGFloat green = (arc4random() % 256)/255.0;
    CGFloat blue  = (arc4random() % 256)/255.0;
    CGFloat alpha = 1.0f;
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

@end
